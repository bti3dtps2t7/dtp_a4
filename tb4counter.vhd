
library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic

entity tb4counter is
end entity tb4counter;


architecture beh of tb4counter is
    signal ValLd_s : std_logic_vector(11 downto 0);
    signal up_s : std_logic;
    signal down_s : std_logic;
    signal nLd_s : std_logic;
    signal clk_s : std_logic;
    signal nres_s : std_logic;
    signal err_arop_s : std_logic;
    signal cnt_arop_s : std_logic_vector( 11 downto 0 );
    signal nClr_arop_s : std_logic;


    component sg4counter is
        port(
            sg_ValLd : out std_logic_vector(11 downto 0);
            sg_up : out std_logic;
            sg_down : out std_logic;
            sg_nLd : out std_logic;
            sg_clk : out std_logic;
            sg_nres : out std_logic;
            sg_nClr : out std_logic
        );--]port
    end component sg4counter;
    for all : sg4counter use entity work.sg4counter( beh );

    component counter is
    port ( 
        err   : out std_logic;                        --ERRor : invalid counter value
        cnt   : out std_logic_vector( 11 downto 0 );  --CouNT
        valLd : in  std_logic_vector( 11 downto 0 );  --init VALue in case of LoaD
        nLd   : in  std_logic;                        --Not LoaD; low actve LoaD
        up    : in  std_logic;                        --UP count command
        down  : in  std_logic;                        --DOWN count command
        clk   : in  std_logic;                        --CLocK
        nres  : in  std_logic;                         --Not RESet ; low active synchronous reset
        nClr : in std_logic -- Not CLeaR : low actve CLeaR
    );--]port
    end component counter;
    for all : counter use entity work.counter( Structure );
begin

sg_i : sg4counter
        port map (
            sg_ValLd => ValLd_s,
            sg_up => up_s,
            sg_down => down_s,
            sg_nLd => nLd_s,
            sg_clk => clk_s,
            sg_nres => nres_s,
            sg_nClr => nClr_arop_s
        )--]port
    ;--]sg_i


cnt_1 : counter
    port map (
        valLd => ValLd_s,
        nLd => nLd_s,
        up => up_s,
        down => down_s,
        clk => clk_s,
        nres => nres_s,
        err => err_arop_s,
        cnt => cnt_arop_s,
        nClr => nClr_arop_s
    )--]port
;--]ha_i1

end architecture beh;