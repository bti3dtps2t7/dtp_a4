
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


entity counter is -- Device Under test
    port (
    err : out std_logic; -- ERRor : invalid counter value
    cnt : out std_logic_vector( 11 downto 0 ); -- CouNT
    --
    valLd : in std_logic_vector( 11 downto 0 ); -- init VALue in case of LoaD
    nLd : in std_logic; -- Not LoaD; low actve LoaD
    nClr : in std_logic; -- Not CLeaR : low actve CLeaR
    up : in std_logic; -- UP count command
    down : in std_logic; -- DOWN count command
    --
    clk : in std_logic; -- CLocK
    nres : in std_logic -- Not RESet ; low active synchronous reset
    );--]port
end entity counter;


architecture arop of counter is
    signal cnt_ns : std_logic_vector(11 downto 0);
    signal valLd_ns : std_logic_vector(11 downto 0);
    signal err_ns : std_logic;
    signal up_ns : std_logic;
    signal down_ns : std_logic;
    signal nLd_ns : std_logic;
    signal nClr_ns : std_logic;

    signal cnt_cs : std_logic_vector(11 downto 0):= (others=>'0');
    signal valLd_cs : std_logic_vector(11 downto 0):= (others=>'0');
    signal err_cs : std_logic := '0';
    signal up_cs : std_logic := '0';
    signal down_cs : std_logic := '0';
    signal nLd_cs : std_logic := '0';
    signal nClr_cs : std_logic := '1';

    signal up_new_cs : std_logic:= '0';
    signal up_old_cs : std_logic:= '0';
    signal down_new_cs : std_logic:= '0';
    signal down_old_cs : std_logic:= '0';
    signal nLd_new_cs : std_logic:= '1';
    signal nLd_old_cs : std_logic:= '1';
    signal nClr_new_cs : std_logic:= '1';
    signal nClr_old_cs : std_logic:= '1';

begin

    up_ns <= up;
    down_ns <= down;
    valLd_ns <= valLd;
    nLd_ns <= nLd;
    nClr_ns <= nClr;

    up_cs <= up_new_cs and not up_old_cs;
    down_cs <= down_new_cs and not down_old_cs;
    nLd_cs <= not nLd_new_cs nand nLd_old_cs;
    nClr_cs <= not nClr_new_cs nand nClr_old_cs;

reg:
    process (clk) is
    begin
        if clk = '1' and clk'event then
            if nres = '0' then
                cnt_cs <= (others=>'0');
                valLd_cs <= (others=>'0');
                err_cs <= '0';

                up_new_cs <= '0';
                up_old_cs <= '0';

                down_new_cs <= '0';
                down_old_cs <= '0';

                nLd_new_cs <= '1';
                nLd_old_cs <= '1';

                nClr_new_cs <= '1';
                nClr_old_cs <= '1';

            else
                cnt_cs <= cnt_ns;
                err_cs <= err_ns;

                up_new_cs <= up_ns;
                up_old_cs <= up_new_cs;

                down_new_cs <= down_ns;
                down_old_cs <= down_new_cs;

                nLd_new_cs <= nLd_ns;
                nLd_old_cs <= nLd_new_cs;

                nClr_new_cs <= nClr_ns;
                nClr_old_cs <= nClr_new_cs;

                valLd_cs <= valLd_ns;
                nLd_cs <= nLd_ns;
                nClr_cs <= nClr_ns;
            end if;
        end if;
    end process reg;


counter:
    process (up_cs, down_cs, cnt_cs, valLd_cs, nLd_cs, err_cs, nClr_cs) is
        variable cnt_v : std_logic_vector(11 downto 0);
        variable down_v : std_logic;
        variable up_v : std_logic;
        variable err_v : std_logic;
        variable nLd_v : std_logic;
        variable cnt_new_v : std_logic_vector(11 downto 0);
        variable nClr_v : std_logic;
        variable summand_v : std_logic_vector(11 downto 0);
    begin
        up_v := up_cs;
        down_v := down_cs;
        err_v := err_cs;
        nLd_v := nLd_cs;
        nClr_v := nClr_cs;
        summand_v := (others => '0');

        if nLd_v = '0' then
            cnt_v := valLd_cs;
            cnt_new_v := valLd_cs;
        else
            cnt_v := cnt_cs;
            cnt_new_v := cnt_cs;
        end if;

        if nClr_v = '0' then 
            err_v := '0';
        end if;

        if up_v = '1' and down_v = '0' then
            summand_v := x"001";
        elsif up_v = '0' and down_v = '1' then
            summand_v := x"FFF";
        end if;

        cnt_new_v := cnt_v + summand_v;

        if up_v = '1' and down_v = '0' then
            if cnt_v > cnt_new_v then
                err_v := '1';
            end if;
        elsif up_v = '0' and down_v = '1' then
            if cnt_v < cnt_new_v then
                err_v := '1';
            end if;
        end if;

        cnt_ns <= cnt_new_v;
        err_ns <= err_v;
    end process counter;

    cnt <= cnt_cs;
    err <= err_cs;

end architecture arop;