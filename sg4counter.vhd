library work;
    use work.all;

library std;
    use std.textio.all;                 -- benoetigt vom "markStart"-PROCESS

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;       -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")
    use ieee.std_logic_unsigned.all;    -- benoetigt fuer conv_std_logic_vector() (und Entscheidung gegen "numeric_std_unsigned")


entity sg4counter is
    port (
        sg_ValLd : out std_logic_vector(11 downto 0);
        sg_up : out std_logic := '0';
        sg_down : out std_logic := '0';
        sg_nLd : out std_logic := '1';
        sg_clk : out std_logic;
        sg_nres : out std_logic;
        sg_nClr : out std_logic := '1'
    );--]port
end entity sg4counter;

architecture beh of sg4counter is
    
    -- Das Arbeiten mit 1/8 Takt-Zyklen ist eigentlich "uebertrieben",
    --   aber es erleichtert das Lesen der Waves
    --   und es bleibt auf den Stimuli Generator beschraenkt
    -- 8*2500ps = 20ns Takt-Periode => 50MHz Takt-Frequenz
    constant oneEigthClockCycle     : time  := 10000 ps;
    constant sevenEigthClockCycle   : time  := 7 * oneEigthClockCycle;
    constant halfClockCycle         : time  := 4 * oneEigthClockCycle;
    constant fullClockCycle         : time  := 8 * oneEigthClockCycle;
    constant doubleClockCycle       : time  := 16 * oneEigthClockCycle;
    
    
    signal   simulationRunning_s    : boolean  := true;     -- for internal signalling that simulation is running 
    
    signal   VDD                    : std_logic  := '1';    -- always one
    signal   GND                    : std_logic  := '0';    -- always zero
    
    signal   nres_s                 : std_logic  := 'L';    -- "internal" nres - workaround to enable reading nres without using "buffer-ports"
    signal   clk_s                  : std_logic;            -- "internal" clk - workaround to enable reading clk without using "buffer-ports"
    
begin
    
    VDD <= '1';
    GND <= '0';

       
    
    -- Der folgende PROCESS markiert im Transcript-Fenster das Ende des RESETs
    -- In der Konsequenz muessen dann alle Signale initialisiert und "wohl-definiert" sein
    markStart:                                              -- mark that REST has passed and hence, all signals have to be valid
    process is
        variable wlb : line;
    begin
        -- wait for RESET to appear
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        -- (low active) RESET is active
        --
        -- wait for RESET to vanish
        wait until nres_s='1';
        -- RESET has passed
        
        -- mark that all signals have to be "valid"
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###############################################################################" ) );     writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        write( wlb, string'( "###   RESET has passed - all signals have to be valid  @ " ) );
        write( wlb, now );                                                                                              writeline( output, wlb );
        write( wlb, string'( "###" ) );                                                                                 writeline( output, wlb );
        
        wait;
    end process markStart;



    resGen:                                                 -- RESet GENerator
    process is
    begin
        nres_s <= '0';                                      -- set low active reset
        for i in 1 to 2 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- since 2 rising clk edges have passed, synchronous reset must have been executed
        
        wait for oneEigthClockCycle;
        nres_s <= '1';                                      -- clear low active reset
        -- 1/8 clk period after rising clk edge reset is vanishing
        
        wait;
    end process resGen;
    --
    sg_nres <= nres_s;
    
    
    
    
    
    clkGen:                                                 -- CLocK GENerator
    process is
    begin
        clk_s <= '0';
        wait for fullClockCycle;
        while simulationRunning_s loop
            clk_s <= '1';
            wait for halfClockCycle;
            clk_s <= '0';
            wait for halfClockCycle;
        end loop;
        wait;
    end process clkGen;
    --
    sg_clk <= clk_s;
    
    
    
    
    
    sg:                                                     -- Stimuli Generator
    process is
    begin
        simulationRunning_s <= true;                        -- redundant as result of power-up initialisation - but however, it looks nicer
        
        if  nres_s/='0'  then  wait until nres_s='0';  end if;
        sg_ValLd <= (others=>'0');                              -- just set any defined data
        if  nres_s/='1'  then  wait until nres_s='1';  end if;
        --reset has passed
        
        
        
        -- give CPLD some time to wake up
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        -- CPLD is configured and able to react
        

        wait for oneEigthClockCycle;
        sg_ValLd <= (others=>'0');
        sg_nLd <= '0';
        wait until '1'=clk_s and clk_s'event;
        wait for oneEigthClockCycle;
        sg_nLd <= '1';
        wait until '1'=clk_s and clk_s'event;

        wait for oneEigthClockCycle;

        for i in 0 to 15 loop
            sg_up <= '1';
            wait for doubleClockCycle;
            sg_up <= '0';
            wait for doubleClockCycle;
        end loop;

        wait for oneEigthClockCycle;
        sg_up <= '0';
        sg_down <= '0';

        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;


        wait for oneEigthClockCycle;
        sg_ValLd <= conv_std_logic_vector(55 , 12);
        sg_nLd <= '0';
        wait until '1'=clk_s and clk_s'event;
        wait for oneEigthClockCycle;
        sg_nLd <= '1';
        wait until '1'=clk_s and clk_s'event;
        
        wait for oneEigthClockCycle;

        for i in 0 to 15 loop
            sg_down <= '1';
            wait for doubleClockCycle;
            sg_down <= '0';
            wait for doubleClockCycle;
        end loop;

        wait for oneEigthClockCycle;
        sg_up <= '0';
        sg_down <= '0';

        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;

        wait for oneEigthClockCycle;
        sg_ValLd <= conv_std_logic_vector(4095 , 12);
        sg_nLd <= '0';
        wait until '1'=clk_s and clk_s'event;
        wait for oneEigthClockCycle;
        sg_nLd <= '1';
        wait until '1'=clk_s and clk_s'event;
        for i in 0 to 5 loop
            wait for oneEigthClockCycle;
            sg_up <= '1';
            sg_down <= '0';
            wait until '1'=clk_s and clk_s'event;
        end loop;

        wait for oneEigthClockCycle;
        sg_nClr <= '0';
        wait until '1'=clk_s and clk_s'event;
        wait for oneEigthClockCycle;
        sg_nClr <= '1';
        wait until '1'=clk_s and clk_s'event;
        for i in 0 to 5 loop
            wait for oneEigthClockCycle;
            sg_up <= '0';
            sg_down <= '1';
            wait until '1'=clk_s and clk_s'event;
        end loop;
        
        -- stop SG after 10 clk cycles - assuming computed data is stable afterwards
        for  i in 1 to 10 loop
            wait until '1'=clk_s and clk_s'event;           -- wait for rising clock edge
        end loop;
        simulationRunning_s <= false;                       -- stop clk generation
        --
        wait;
    end process sg;
    
end architecture beh;


