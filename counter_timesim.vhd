--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.49d
--  \   \         Application: netgen
--  /   /         Filename: counter_timesim.vhd
-- /___/   /\     Timestamp: Fri Jan 15 13:20:15 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -rpw 100 -ar Structure -tm counter -w -dir netgen/fit -ofmt vhdl -sim counter.nga counter_timesim.vhd 
-- Device	: XC2C256-7-PQ208 (Speed File: Version 14.0 Advance Product Specification)
-- Input file	: counter.nga
-- Output file	: D:\DTP\S2T7\dtp_a4\ISE\ise14x7\netgen\fit\counter_timesim.vhd
-- # of Entities	: 1
-- Design Name	: counter.nga
-- Xilinx	: C:\Xilinx\14.4\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

architecture Structure of counter is
  signal clk_II_FCLK_1 : STD_LOGIC; 
  signal nres_II_UIM_3 : STD_LOGIC; 
  signal down_II_UIM_5 : STD_LOGIC; 
  signal up_II_UIM_7 : STD_LOGIC; 
  signal valLd_0_II_UIM_9 : STD_LOGIC; 
  signal nLd_II_UIM_11 : STD_LOGIC; 
  signal valLd_9_II_UIM_13 : STD_LOGIC; 
  signal valLd_5_II_UIM_15 : STD_LOGIC; 
  signal valLd_1_II_UIM_17 : STD_LOGIC; 
  signal valLd_2_II_UIM_19 : STD_LOGIC; 
  signal valLd_3_II_UIM_21 : STD_LOGIC; 
  signal valLd_4_II_UIM_23 : STD_LOGIC; 
  signal valLd_6_II_UIM_25 : STD_LOGIC; 
  signal valLd_8_II_UIM_27 : STD_LOGIC; 
  signal valLd_7_II_UIM_29 : STD_LOGIC; 
  signal valLd_10_II_UIM_31 : STD_LOGIC; 
  signal valLd_11_II_UIM_33 : STD_LOGIC; 
  signal nClr_II_UIM_35 : STD_LOGIC; 
  signal cnt_0_MC_Q_37 : STD_LOGIC; 
  signal cnt_10_MC_Q_39 : STD_LOGIC; 
  signal cnt_11_MC_Q_41 : STD_LOGIC; 
  signal cnt_1_MC_Q_43 : STD_LOGIC; 
  signal cnt_2_MC_Q_45 : STD_LOGIC; 
  signal cnt_3_MC_Q_47 : STD_LOGIC; 
  signal cnt_4_MC_Q_49 : STD_LOGIC; 
  signal cnt_5_MC_Q_51 : STD_LOGIC; 
  signal cnt_6_MC_Q_53 : STD_LOGIC; 
  signal cnt_7_MC_Q_55 : STD_LOGIC; 
  signal cnt_8_MC_Q_57 : STD_LOGIC; 
  signal cnt_9_MC_Q_59 : STD_LOGIC; 
  signal err_MC_Q_61 : STD_LOGIC; 
  signal cnt_0_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_0_MC_UIM_63 : STD_LOGIC; 
  signal cnt_0_MC_D_64 : STD_LOGIC; 
  signal Gnd_65 : STD_LOGIC; 
  signal Vcc_66 : STD_LOGIC; 
  signal cnt_0_MC_D1_67 : STD_LOGIC; 
  signal cnt_0_MC_D2_68 : STD_LOGIC; 
  signal N_PZ_137_70 : STD_LOGIC; 
  signal N_PZ_133_72 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_0_73 : STD_LOGIC; 
  signal cnt_0_MC_D2_PT_1_74 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_Q_75 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_D_76 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_D1_77 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_D2_78 : STD_LOGIC; 
  signal down_new_cs_79 : STD_LOGIC; 
  signal up_new_cs_80 : STD_LOGIC; 
  signal down_old_cs_81 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_D2_PT_0_82 : STD_LOGIC; 
  signal up_old_cs_83 : STD_LOGIC; 
  signal summand_v_mux0001_10_MC_D2_PT_1_84 : STD_LOGIC; 
  signal down_new_cs_MC_Q : STD_LOGIC; 
  signal down_new_cs_MC_D_86 : STD_LOGIC; 
  signal down_new_cs_MC_D1_87 : STD_LOGIC; 
  signal down_new_cs_MC_D2_88 : STD_LOGIC; 
  signal up_new_cs_MC_Q : STD_LOGIC; 
  signal up_new_cs_MC_D_90 : STD_LOGIC; 
  signal up_new_cs_MC_D1_91 : STD_LOGIC; 
  signal up_new_cs_MC_D2_92 : STD_LOGIC; 
  signal down_old_cs_MC_Q : STD_LOGIC; 
  signal down_old_cs_MC_D_94 : STD_LOGIC; 
  signal down_old_cs_MC_D1_95 : STD_LOGIC; 
  signal down_old_cs_MC_D2_96 : STD_LOGIC; 
  signal up_old_cs_MC_Q : STD_LOGIC; 
  signal up_old_cs_MC_D_98 : STD_LOGIC; 
  signal up_old_cs_MC_D1_99 : STD_LOGIC; 
  signal up_old_cs_MC_D2_100 : STD_LOGIC; 
  signal N_PZ_137_MC_Q_101 : STD_LOGIC; 
  signal N_PZ_137_MC_D_102 : STD_LOGIC; 
  signal N_PZ_137_MC_D1_103 : STD_LOGIC; 
  signal N_PZ_137_MC_D2_104 : STD_LOGIC; 
  signal N_PZ_137_MC_D2_PT_0_105 : STD_LOGIC; 
  signal N_PZ_137_MC_D2_PT_1_106 : STD_LOGIC; 
  signal valLd_cs_0_MC_Q : STD_LOGIC; 
  signal valLd_cs_0_MC_D_108 : STD_LOGIC; 
  signal valLd_cs_0_MC_D1_109 : STD_LOGIC; 
  signal valLd_cs_0_MC_D2_110 : STD_LOGIC; 
  signal N_PZ_133_MC_Q_111 : STD_LOGIC; 
  signal N_PZ_133_MC_D_112 : STD_LOGIC; 
  signal N_PZ_133_MC_D1_113 : STD_LOGIC; 
  signal N_PZ_133_MC_D2_114 : STD_LOGIC; 
  signal nLd_new_cs_115 : STD_LOGIC; 
  signal nLd_old_cs_116 : STD_LOGIC; 
  signal nLd_new_cs_MC_Q : STD_LOGIC; 
  signal nLd_new_cs_MC_D_118 : STD_LOGIC; 
  signal nLd_new_cs_MC_D1_119 : STD_LOGIC; 
  signal nLd_new_cs_MC_D2_120 : STD_LOGIC; 
  signal nLd_old_cs_MC_Q : STD_LOGIC; 
  signal nLd_old_cs_MC_D_122 : STD_LOGIC; 
  signal nLd_old_cs_MC_D1_123 : STD_LOGIC; 
  signal nLd_old_cs_MC_D2_124 : STD_LOGIC; 
  signal cnt_10_MC_Q_tsimrenamed_net_Q_125 : STD_LOGIC; 
  signal cnt_10_MC_D_126 : STD_LOGIC; 
  signal cnt_10_MC_D1_127 : STD_LOGIC; 
  signal cnt_10_MC_D2_128 : STD_LOGIC; 
  signal cnt_10_BUFR_129 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D_131 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D1_132 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_133 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_0_135 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_1_138 : STD_LOGIC; 
  signal cnt_10_BUFR_MC_D2_PT_2_139 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_Q_140 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_141 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D1_142 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_143 : STD_LOGIC; 
  signal cnt_v_mux0000_9_Q_144 : STD_LOGIC; 
  signal N_PZ_190_145 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_146 : STD_LOGIC; 
  signal cnt_ns_9_Q_147 : STD_LOGIC; 
  signal cnt_v_mux0000_5_Q_148 : STD_LOGIC; 
  signal N_PZ_139_149 : STD_LOGIC; 
  signal N_PZ_253_150 : STD_LOGIC; 
  signal cnt_v_mux0000_8_Q_151 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_152 : STD_LOGIC; 
  signal N_PZ_134_153 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_154 : STD_LOGIC; 
  signal cnt_ns_9_MC_Q_155 : STD_LOGIC; 
  signal cnt_ns_9_MC_D_156 : STD_LOGIC; 
  signal cnt_ns_9_MC_D1_157 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_158 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_PT_0_159 : STD_LOGIC; 
  signal SF87_160 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_PT_1_161 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_PT_2_162 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_PT_3_163 : STD_LOGIC; 
  signal cnt_ns_9_MC_D2_PT_4_164 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_Q_165 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_D_166 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_D1_167 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_D2_168 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_D2_PT_0_170 : STD_LOGIC; 
  signal cnt_9_BUFR_171 : STD_LOGIC; 
  signal cnt_v_mux0000_9_MC_D2_PT_1_172 : STD_LOGIC; 
  signal valLd_cs_9_MC_Q : STD_LOGIC; 
  signal valLd_cs_9_MC_D_174 : STD_LOGIC; 
  signal valLd_cs_9_MC_D1_175 : STD_LOGIC; 
  signal valLd_cs_9_MC_D2_176 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D_178 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D1_179 : STD_LOGIC; 
  signal cnt_9_BUFR_MC_D2_180 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_Q_181 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_D_182 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_D1_183 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_D2_184 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_D2_PT_0_186 : STD_LOGIC; 
  signal cnt_5_BUFR_187 : STD_LOGIC; 
  signal cnt_v_mux0000_5_MC_D2_PT_1_188 : STD_LOGIC; 
  signal valLd_cs_5_MC_Q : STD_LOGIC; 
  signal valLd_cs_5_MC_D_190 : STD_LOGIC; 
  signal valLd_cs_5_MC_D1_191 : STD_LOGIC; 
  signal valLd_cs_5_MC_D2_192 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D_194 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D1_195 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_196 : STD_LOGIC; 
  signal N_PZ_138_197 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_0_198 : STD_LOGIC; 
  signal cnt_5_BUFR_MC_D2_PT_1_199 : STD_LOGIC; 
  signal N_PZ_138_MC_Q_200 : STD_LOGIC; 
  signal N_PZ_138_MC_D_201 : STD_LOGIC; 
  signal N_PZ_138_MC_D1_202 : STD_LOGIC; 
  signal N_PZ_138_MC_D2_203 : STD_LOGIC; 
  signal SF4_204 : STD_LOGIC; 
  signal N_PZ_135_205 : STD_LOGIC; 
  signal N_PZ_138_MC_D2_PT_0_206 : STD_LOGIC; 
  signal err_cs_and000072_207 : STD_LOGIC; 
  signal N_PZ_138_MC_D2_PT_1_208 : STD_LOGIC; 
  signal err_cs_and000072_MC_Q_209 : STD_LOGIC; 
  signal err_cs_and000072_MC_D_210 : STD_LOGIC; 
  signal err_cs_and000072_MC_D1_211 : STD_LOGIC; 
  signal err_cs_and000072_MC_D2_212 : STD_LOGIC; 
  signal err_cs_and000072_MC_D2_PT_0_213 : STD_LOGIC; 
  signal cnt_2_MC_UIM_214 : STD_LOGIC; 
  signal N_PZ_294_215 : STD_LOGIC; 
  signal err_cs_and000072_MC_D2_PT_1_216 : STD_LOGIC; 
  signal err_cs_and000072_MC_D2_PT_2_220 : STD_LOGIC; 
  signal SF4_MC_Q_221 : STD_LOGIC; 
  signal SF4_MC_D_222 : STD_LOGIC; 
  signal SF4_MC_D1_223 : STD_LOGIC; 
  signal SF4_MC_D2_224 : STD_LOGIC; 
  signal cnt_3_MC_UIM_225 : STD_LOGIC; 
  signal SF4_MC_D2_PT_0_226 : STD_LOGIC; 
  signal SF4_MC_D2_PT_1_227 : STD_LOGIC; 
  signal cnt_1_MC_UIM_228 : STD_LOGIC; 
  signal SF4_MC_D2_PT_2_229 : STD_LOGIC; 
  signal SF4_MC_D2_PT_3_230 : STD_LOGIC; 
  signal valLd_cs_1_MC_Q : STD_LOGIC; 
  signal valLd_cs_1_MC_D_232 : STD_LOGIC; 
  signal valLd_cs_1_MC_D1_233 : STD_LOGIC; 
  signal valLd_cs_1_MC_D2_234 : STD_LOGIC; 
  signal valLd_cs_2_MC_Q : STD_LOGIC; 
  signal valLd_cs_2_MC_D_236 : STD_LOGIC; 
  signal valLd_cs_2_MC_D1_237 : STD_LOGIC; 
  signal valLd_cs_2_MC_D2_238 : STD_LOGIC; 
  signal valLd_cs_3_MC_Q : STD_LOGIC; 
  signal valLd_cs_3_MC_D_240 : STD_LOGIC; 
  signal valLd_cs_3_MC_D1_241 : STD_LOGIC; 
  signal valLd_cs_3_MC_D2_242 : STD_LOGIC; 
  signal cnt_1_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_1_MC_D_244 : STD_LOGIC; 
  signal cnt_1_MC_tsimcreated_xor_Q_245 : STD_LOGIC; 
  signal cnt_1_MC_D1_246 : STD_LOGIC; 
  signal cnt_1_MC_D2_247 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_0_248 : STD_LOGIC; 
  signal N_PZ_232_249 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_1_250 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_2_251 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_3_252 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_4_253 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_5_254 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_6_255 : STD_LOGIC; 
  signal cnt_1_MC_D2_PT_7_256 : STD_LOGIC; 
  signal N_PZ_232_MC_Q_257 : STD_LOGIC; 
  signal N_PZ_232_MC_D_258 : STD_LOGIC; 
  signal N_PZ_232_MC_D1_259 : STD_LOGIC; 
  signal N_PZ_232_MC_D2_260 : STD_LOGIC; 
  signal N_PZ_232_MC_D2_PT_0_261 : STD_LOGIC; 
  signal N_PZ_232_MC_D2_PT_1_262 : STD_LOGIC; 
  signal cnt_2_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_2_MC_D_264 : STD_LOGIC; 
  signal cnt_2_MC_D1_265 : STD_LOGIC; 
  signal cnt_2_MC_D2_266 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_0_267 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_1_268 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_2_269 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_3_270 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_4_271 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_5_272 : STD_LOGIC; 
  signal cnt_2_MC_D2_PT_6_273 : STD_LOGIC; 
  signal N_PZ_294_MC_Q_274 : STD_LOGIC; 
  signal N_PZ_294_MC_D_275 : STD_LOGIC; 
  signal N_PZ_294_MC_D1_276 : STD_LOGIC; 
  signal N_PZ_294_MC_D2_277 : STD_LOGIC; 
  signal cnt_3_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal cnt_3_MC_D_279 : STD_LOGIC; 
  signal cnt_3_MC_D1_280 : STD_LOGIC; 
  signal cnt_3_MC_D2_281 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_0_282 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_1_283 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_2_284 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_3_285 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_4_286 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_5_287 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_6_288 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_7_289 : STD_LOGIC; 
  signal cnt_3_MC_D2_PT_8_290 : STD_LOGIC; 
  signal N_PZ_135_MC_Q_291 : STD_LOGIC; 
  signal N_PZ_135_MC_D_292 : STD_LOGIC; 
  signal N_PZ_135_MC_D1_293 : STD_LOGIC; 
  signal N_PZ_135_MC_D2_294 : STD_LOGIC; 
  signal N_PZ_135_MC_D2_PT_0_296 : STD_LOGIC; 
  signal cnt_4_BUFR_297 : STD_LOGIC; 
  signal N_PZ_135_MC_D2_PT_1_298 : STD_LOGIC; 
  signal valLd_cs_4_MC_Q : STD_LOGIC; 
  signal valLd_cs_4_MC_D_300 : STD_LOGIC; 
  signal valLd_cs_4_MC_D1_301 : STD_LOGIC; 
  signal valLd_cs_4_MC_D2_302 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D_304 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D1_305 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_306 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_0_307 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_1_308 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_2_309 : STD_LOGIC; 
  signal cnt_4_BUFR_MC_D2_PT_3_310 : STD_LOGIC; 
  signal SF87_MC_Q_311 : STD_LOGIC; 
  signal SF87_MC_D_312 : STD_LOGIC; 
  signal SF87_MC_D1_313 : STD_LOGIC; 
  signal SF87_MC_D2_314 : STD_LOGIC; 
  signal SF87_MC_D2_PT_0_315 : STD_LOGIC; 
  signal SF87_MC_D2_PT_1_316 : STD_LOGIC; 
  signal N_PZ_139_MC_Q_317 : STD_LOGIC; 
  signal N_PZ_139_MC_D_318 : STD_LOGIC; 
  signal N_PZ_139_MC_D1_319 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_320 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_PT_0_322 : STD_LOGIC; 
  signal cnt_6_BUFR_323 : STD_LOGIC; 
  signal N_PZ_139_MC_D2_PT_1_324 : STD_LOGIC; 
  signal valLd_cs_6_MC_Q : STD_LOGIC; 
  signal valLd_cs_6_MC_D_326 : STD_LOGIC; 
  signal valLd_cs_6_MC_D1_327 : STD_LOGIC; 
  signal valLd_cs_6_MC_D2_328 : STD_LOGIC; 
  signal cnt_6_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_6_BUFR_MC_D_330 : STD_LOGIC; 
  signal cnt_6_BUFR_MC_D1_331 : STD_LOGIC; 
  signal cnt_6_BUFR_MC_D2_332 : STD_LOGIC; 
  signal cnt_6_BUFR_MC_D2_PT_0_333 : STD_LOGIC; 
  signal cnt_6_BUFR_MC_D2_PT_1_334 : STD_LOGIC; 
  signal N_PZ_253_MC_Q_335 : STD_LOGIC; 
  signal N_PZ_253_MC_D_336 : STD_LOGIC; 
  signal N_PZ_253_MC_D1_337 : STD_LOGIC; 
  signal N_PZ_253_MC_D2_338 : STD_LOGIC; 
  signal N_PZ_253_MC_D2_PT_0_339 : STD_LOGIC; 
  signal N_PZ_253_MC_D2_PT_1_340 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_Q_341 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_D_342 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_D1_343 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_D2_344 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_D2_PT_0_346 : STD_LOGIC; 
  signal cnt_8_BUFR_347 : STD_LOGIC; 
  signal cnt_v_mux0000_8_MC_D2_PT_1_348 : STD_LOGIC; 
  signal valLd_cs_8_MC_Q : STD_LOGIC; 
  signal valLd_cs_8_MC_D_350 : STD_LOGIC; 
  signal valLd_cs_8_MC_D1_351 : STD_LOGIC; 
  signal valLd_cs_8_MC_D2_352 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D_354 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D1_355 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_356 : STD_LOGIC; 
  signal cnt_ns_7_Q_357 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_0_358 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_1_359 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_2_360 : STD_LOGIC; 
  signal cnt_8_BUFR_MC_D2_PT_3_361 : STD_LOGIC; 
  signal cnt_ns_7_MC_Q_362 : STD_LOGIC; 
  signal cnt_ns_7_MC_D_363 : STD_LOGIC; 
  signal cnt_ns_7_MC_D1_364 : STD_LOGIC; 
  signal cnt_ns_7_MC_D2_365 : STD_LOGIC; 
  signal cnt_ns_7_MC_D2_PT_0_366 : STD_LOGIC; 
  signal cnt_ns_7_MC_D2_PT_1_367 : STD_LOGIC; 
  signal cnt_ns_7_MC_D2_PT_2_368 : STD_LOGIC; 
  signal cnt_ns_7_MC_D2_PT_3_369 : STD_LOGIC; 
  signal N_PZ_134_MC_Q_370 : STD_LOGIC; 
  signal N_PZ_134_MC_D_371 : STD_LOGIC; 
  signal N_PZ_134_MC_D1_372 : STD_LOGIC; 
  signal N_PZ_134_MC_D2_373 : STD_LOGIC; 
  signal N_PZ_134_MC_D2_PT_0_375 : STD_LOGIC; 
  signal cnt_7_BUFR_376 : STD_LOGIC; 
  signal N_PZ_134_MC_D2_PT_1_377 : STD_LOGIC; 
  signal valLd_cs_7_MC_Q : STD_LOGIC; 
  signal valLd_cs_7_MC_D_379 : STD_LOGIC; 
  signal valLd_cs_7_MC_D1_380 : STD_LOGIC; 
  signal valLd_cs_7_MC_D2_381 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D_383 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D1_384 : STD_LOGIC; 
  signal cnt_7_BUFR_MC_D2_385 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_Q_386 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_387 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D1_388 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_389 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_390 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_391 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_392 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_393 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_394 : STD_LOGIC; 
  signal Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_395 : STD_LOGIC; 
  signal valLd_cs_10_MC_Q : STD_LOGIC; 
  signal valLd_cs_10_MC_D_397 : STD_LOGIC; 
  signal valLd_cs_10_MC_D1_398 : STD_LOGIC; 
  signal valLd_cs_10_MC_D2_399 : STD_LOGIC; 
  signal N_PZ_190_MC_Q_400 : STD_LOGIC; 
  signal N_PZ_190_MC_D_401 : STD_LOGIC; 
  signal N_PZ_190_MC_D1_402 : STD_LOGIC; 
  signal N_PZ_190_MC_D2_403 : STD_LOGIC; 
  signal N_PZ_190_MC_D2_PT_0_404 : STD_LOGIC; 
  signal N_PZ_190_MC_D2_PT_1_405 : STD_LOGIC; 
  signal N_PZ_190_MC_D2_PT_2_406 : STD_LOGIC; 
  signal cnt_11_MC_Q_tsimrenamed_net_Q_407 : STD_LOGIC; 
  signal cnt_11_MC_D_408 : STD_LOGIC; 
  signal cnt_11_MC_D1_409 : STD_LOGIC; 
  signal cnt_11_MC_D2_410 : STD_LOGIC; 
  signal cnt_11_BUFR_411 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_Q : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D_413 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D1_414 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D2_415 : STD_LOGIC; 
  signal N_PZ_266_416 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D2_PT_0_417 : STD_LOGIC; 
  signal N_PZ_155_419 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D2_PT_1_420 : STD_LOGIC; 
  signal cnt_11_BUFR_MC_D2_PT_2_421 : STD_LOGIC; 
  signal valLd_cs_11_MC_Q : STD_LOGIC; 
  signal valLd_cs_11_MC_D_423 : STD_LOGIC; 
  signal valLd_cs_11_MC_D1_424 : STD_LOGIC; 
  signal valLd_cs_11_MC_D2_425 : STD_LOGIC; 
  signal N_PZ_155_MC_Q_426 : STD_LOGIC; 
  signal N_PZ_155_MC_D_427 : STD_LOGIC; 
  signal N_PZ_155_MC_D1_428 : STD_LOGIC; 
  signal N_PZ_155_MC_D2_429 : STD_LOGIC; 
  signal N_PZ_155_MC_D2_PT_0_430 : STD_LOGIC; 
  signal N_PZ_155_MC_D2_PT_1_431 : STD_LOGIC; 
  signal N_PZ_266_MC_Q_432 : STD_LOGIC; 
  signal N_PZ_266_MC_D_433 : STD_LOGIC; 
  signal N_PZ_266_MC_D1_434 : STD_LOGIC; 
  signal N_PZ_266_MC_D2_435 : STD_LOGIC; 
  signal N_PZ_266_MC_D2_PT_0_436 : STD_LOGIC; 
  signal N_PZ_266_MC_D2_PT_1_437 : STD_LOGIC; 
  signal cnt_4_MC_Q_tsimrenamed_net_Q_438 : STD_LOGIC; 
  signal cnt_4_MC_D_439 : STD_LOGIC; 
  signal cnt_4_MC_D1_440 : STD_LOGIC; 
  signal cnt_4_MC_D2_441 : STD_LOGIC; 
  signal cnt_5_MC_Q_tsimrenamed_net_Q_442 : STD_LOGIC; 
  signal cnt_5_MC_D_443 : STD_LOGIC; 
  signal cnt_5_MC_D1_444 : STD_LOGIC; 
  signal cnt_5_MC_D2_445 : STD_LOGIC; 
  signal cnt_6_MC_Q_tsimrenamed_net_Q_446 : STD_LOGIC; 
  signal cnt_6_MC_D_447 : STD_LOGIC; 
  signal cnt_6_MC_D1_448 : STD_LOGIC; 
  signal cnt_6_MC_D2_449 : STD_LOGIC; 
  signal cnt_7_MC_Q_tsimrenamed_net_Q_450 : STD_LOGIC; 
  signal cnt_7_MC_D_451 : STD_LOGIC; 
  signal cnt_7_MC_D1_452 : STD_LOGIC; 
  signal cnt_7_MC_D2_453 : STD_LOGIC; 
  signal cnt_8_MC_Q_tsimrenamed_net_Q_454 : STD_LOGIC; 
  signal cnt_8_MC_D_455 : STD_LOGIC; 
  signal cnt_8_MC_D1_456 : STD_LOGIC; 
  signal cnt_8_MC_D2_457 : STD_LOGIC; 
  signal cnt_9_MC_Q_tsimrenamed_net_Q_458 : STD_LOGIC; 
  signal cnt_9_MC_D_459 : STD_LOGIC; 
  signal cnt_9_MC_D1_460 : STD_LOGIC; 
  signal cnt_9_MC_D2_461 : STD_LOGIC; 
  signal err_MC_Q_tsimrenamed_net_Q : STD_LOGIC; 
  signal err_MC_UIM_463 : STD_LOGIC; 
  signal err_MC_D_464 : STD_LOGIC; 
  signal err_MC_D1_465 : STD_LOGIC; 
  signal err_MC_D2_466 : STD_LOGIC; 
  signal N_PZ_247_467 : STD_LOGIC; 
  signal err_MC_D2_PT_0_468 : STD_LOGIC; 
  signal N_PZ_264_469 : STD_LOGIC; 
  signal err_MC_D2_PT_1_470 : STD_LOGIC; 
  signal err_cs_or0003_471 : STD_LOGIC; 
  signal err_MC_D2_PT_2_472 : STD_LOGIC; 
  signal err_MC_D2_PT_3_473 : STD_LOGIC; 
  signal err_cs_or000367_474 : STD_LOGIC; 
  signal err_MC_D2_PT_4_475 : STD_LOGIC; 
  signal err_MC_D2_PT_5_476 : STD_LOGIC; 
  signal err_MC_D2_PT_6_477 : STD_LOGIC; 
  signal err_MC_D2_PT_7_478 : STD_LOGIC; 
  signal N_PZ_163_479 : STD_LOGIC; 
  signal N_PZ_202_480 : STD_LOGIC; 
  signal err_MC_D2_PT_8_481 : STD_LOGIC; 
  signal err_MC_D2_PT_9_482 : STD_LOGIC; 
  signal err_MC_D2_PT_10_483 : STD_LOGIC; 
  signal err_MC_D2_PT_11_484 : STD_LOGIC; 
  signal err_MC_D2_PT_12_485 : STD_LOGIC; 
  signal err_MC_D2_PT_13_486 : STD_LOGIC; 
  signal err_MC_D2_PT_14_487 : STD_LOGIC; 
  signal N_PZ_247_MC_Q_488 : STD_LOGIC; 
  signal N_PZ_247_MC_D_489 : STD_LOGIC; 
  signal N_PZ_247_MC_D1_490 : STD_LOGIC; 
  signal N_PZ_247_MC_D2_491 : STD_LOGIC; 
  signal N_PZ_247_MC_D2_PT_0_492 : STD_LOGIC; 
  signal N_PZ_247_MC_D2_PT_1_493 : STD_LOGIC; 
  signal N_PZ_247_MC_D2_PT_2_494 : STD_LOGIC; 
  signal N_PZ_247_MC_D2_PT_3_495 : STD_LOGIC; 
  signal err_cs_or0003_MC_Q_496 : STD_LOGIC; 
  signal err_cs_or0003_MC_D_497 : STD_LOGIC; 
  signal err_cs_or0003_MC_D1_498 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_499 : STD_LOGIC; 
  signal nClr_new_cs_500 : STD_LOGIC; 
  signal nClr_old_cs_501 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_0_502 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_1_503 : STD_LOGIC; 
  signal N_PZ_250_504 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_2_505 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_3_506 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_4_507 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_5_508 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_6_509 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_7_510 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_8_511 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_9_512 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_10_513 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_11_514 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_12_515 : STD_LOGIC; 
  signal err_cs_or0003_MC_D2_PT_13_516 : STD_LOGIC; 
  signal nClr_new_cs_MC_Q : STD_LOGIC; 
  signal nClr_new_cs_MC_D_518 : STD_LOGIC; 
  signal nClr_new_cs_MC_D1_519 : STD_LOGIC; 
  signal nClr_new_cs_MC_D2_520 : STD_LOGIC; 
  signal nClr_old_cs_MC_Q : STD_LOGIC; 
  signal nClr_old_cs_MC_D_522 : STD_LOGIC; 
  signal nClr_old_cs_MC_D1_523 : STD_LOGIC; 
  signal nClr_old_cs_MC_D2_524 : STD_LOGIC; 
  signal N_PZ_163_MC_Q_525 : STD_LOGIC; 
  signal N_PZ_163_MC_D_526 : STD_LOGIC; 
  signal N_PZ_163_MC_D1_527 : STD_LOGIC; 
  signal N_PZ_163_MC_D2_528 : STD_LOGIC; 
  signal N_PZ_163_MC_D2_PT_0_529 : STD_LOGIC; 
  signal N_PZ_163_MC_D2_PT_1_530 : STD_LOGIC; 
  signal N_PZ_202_MC_Q_531 : STD_LOGIC; 
  signal N_PZ_202_MC_D_532 : STD_LOGIC; 
  signal N_PZ_202_MC_D1_533 : STD_LOGIC; 
  signal N_PZ_202_MC_D2_534 : STD_LOGIC; 
  signal N_PZ_202_MC_D2_PT_0_535 : STD_LOGIC; 
  signal N_PZ_202_MC_D2_PT_1_536 : STD_LOGIC; 
  signal N_PZ_202_MC_D2_PT_2_537 : STD_LOGIC; 
  signal N_PZ_250_MC_Q_538 : STD_LOGIC; 
  signal N_PZ_250_MC_D_539 : STD_LOGIC; 
  signal N_PZ_250_MC_D1_540 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_541 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_PT_0_542 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_PT_1_543 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_PT_2_544 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_PT_3_545 : STD_LOGIC; 
  signal N_PZ_250_MC_D2_PT_4_546 : STD_LOGIC; 
  signal N_PZ_264_MC_Q_547 : STD_LOGIC; 
  signal N_PZ_264_MC_D_548 : STD_LOGIC; 
  signal N_PZ_264_MC_D1_549 : STD_LOGIC; 
  signal N_PZ_264_MC_D2_550 : STD_LOGIC; 
  signal N_PZ_264_MC_D2_PT_0_551 : STD_LOGIC; 
  signal N_PZ_264_MC_D2_PT_1_552 : STD_LOGIC; 
  signal err_cs_or000367_MC_Q_553 : STD_LOGIC; 
  signal err_cs_or000367_MC_D_554 : STD_LOGIC; 
  signal err_cs_or000367_MC_D1_555 : STD_LOGIC; 
  signal err_cs_or000367_MC_D2_556 : STD_LOGIC; 
  signal err_cs_or000367_MC_D2_PT_0_557 : STD_LOGIC; 
  signal err_cs_or000367_MC_D2_PT_1_558 : STD_LOGIC; 
  signal err_cs_or000367_MC_D2_PT_2_559 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_0_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_down_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_up_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_137_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_133_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_133_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_133_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_133_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nLd_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_9_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_138_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_and000072_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF4_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_1_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_3_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_1_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_232_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_2_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_294_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_294_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_294_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_294_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_294_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_6_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_7_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_3_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_135_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_SF87_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_139_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_253_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_ns_7_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_134_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_10_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_190_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_11_BUFR_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_valLd_cs_11_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_155_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_266_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_4_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_5_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_6_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_7_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_8_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_cnt_9_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_11_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_12_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_PT_14_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_247_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN5 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN6 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN7 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN8 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN9 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN10 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN11 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN12 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN13 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN14 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or0003_MC_D2_IN15 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_REG_IN : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_REG_CLK : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_D1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_nClr_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_163_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_202_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_IN3 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_250_MC_D2_IN4 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_N_PZ_264_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_IN0 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_IN1 : STD_LOGIC; 
  signal NlwBufferSignal_err_cs_or000367_MC_D2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_137_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_133_MC_D1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_nLd_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_nLd_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_nLd_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_nLd_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_138_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_138_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_and000072_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_1_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_SF4_MC_D2_PT_3_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_1_MC_D2_PT_7_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_232_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_232_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_6_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_7_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_3_MC_D2_PT_8_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_135_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_SF87_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_SF87_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_SF87_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_SF87_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_SF87_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_139_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_6_BUFR_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_ns_7_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_134_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_cnt_11_BUFR_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_155_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_5_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_6_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_7_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_10_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_10_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_11_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_12_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_12_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_12_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_13_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_13_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_14_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_14_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_14_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_14_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_MC_D2_PT_14_IN7 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_1_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_0_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_5_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN5 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN6 : STD_LOGIC; 
  signal NlwInverterSignal_nClr_new_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_nClr_new_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_nClr_old_cs_MC_D_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_nClr_old_cs_MC_D1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_202_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_202_MC_D2_PT_2_IN2 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_250_MC_D2_PT_1_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_250_MC_D2_PT_2_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_250_MC_D2_PT_3_IN3 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_250_MC_D2_PT_4_IN4 : STD_LOGIC; 
  signal NlwInverterSignal_N_PZ_264_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or000367_MC_D2_PT_0_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or000367_MC_D2_PT_1_IN1 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN0 : STD_LOGIC; 
  signal NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN1 : STD_LOGIC; 
  signal summand_v_mux0001 : STD_LOGIC_VECTOR ( 10 downto 10 ); 
  signal valLd_cs : STD_LOGIC_VECTOR ( 11 downto 0 ); 
begin
  clk_II_FCLK : X_BUF
    port map (
      I => clk,
      O => clk_II_FCLK_1
    );
  nres_II_UIM : X_BUF
    port map (
      I => nres,
      O => nres_II_UIM_3
    );
  down_II_UIM : X_BUF
    port map (
      I => down,
      O => down_II_UIM_5
    );
  up_II_UIM : X_BUF
    port map (
      I => up,
      O => up_II_UIM_7
    );
  valLd_0_II_UIM : X_BUF
    port map (
      I => valLd(0),
      O => valLd_0_II_UIM_9
    );
  nLd_II_UIM : X_BUF
    port map (
      I => nLd,
      O => nLd_II_UIM_11
    );
  valLd_9_II_UIM : X_BUF
    port map (
      I => valLd(9),
      O => valLd_9_II_UIM_13
    );
  valLd_5_II_UIM : X_BUF
    port map (
      I => valLd(5),
      O => valLd_5_II_UIM_15
    );
  valLd_1_II_UIM : X_BUF
    port map (
      I => valLd(1),
      O => valLd_1_II_UIM_17
    );
  valLd_2_II_UIM : X_BUF
    port map (
      I => valLd(2),
      O => valLd_2_II_UIM_19
    );
  valLd_3_II_UIM : X_BUF
    port map (
      I => valLd(3),
      O => valLd_3_II_UIM_21
    );
  valLd_4_II_UIM : X_BUF
    port map (
      I => valLd(4),
      O => valLd_4_II_UIM_23
    );
  valLd_6_II_UIM : X_BUF
    port map (
      I => valLd(6),
      O => valLd_6_II_UIM_25
    );
  valLd_8_II_UIM : X_BUF
    port map (
      I => valLd(8),
      O => valLd_8_II_UIM_27
    );
  valLd_7_II_UIM : X_BUF
    port map (
      I => valLd(7),
      O => valLd_7_II_UIM_29
    );
  valLd_10_II_UIM : X_BUF
    port map (
      I => valLd(10),
      O => valLd_10_II_UIM_31
    );
  valLd_11_II_UIM : X_BUF
    port map (
      I => valLd(11),
      O => valLd_11_II_UIM_33
    );
  nClr_II_UIM : X_BUF
    port map (
      I => nClr,
      O => nClr_II_UIM_35
    );
  cnt_0_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_37,
      O => cnt(0)
    );
  cnt_10_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_39,
      O => cnt(10)
    );
  cnt_11_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_41,
      O => cnt(11)
    );
  cnt_1_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_43,
      O => cnt(1)
    );
  cnt_2_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_45,
      O => cnt(2)
    );
  cnt_3_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_47,
      O => cnt(3)
    );
  cnt_4_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_49,
      O => cnt(4)
    );
  cnt_5_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_51,
      O => cnt(5)
    );
  cnt_6_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_53,
      O => cnt(6)
    );
  cnt_7_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_55,
      O => cnt(7)
    );
  cnt_8_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_57,
      O => cnt(8)
    );
  cnt_9_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_59,
      O => cnt(9)
    );
  err_62 : X_BUF
    port map (
      I => err_MC_Q_61,
      O => err
    );
  cnt_0_MC_Q : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_Q_37
    );
  cnt_0_MC_UIM : X_BUF
    port map (
      I => cnt_0_MC_Q_tsimrenamed_net_Q,
      O => cnt_0_MC_UIM_63
    );
  cnt_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_0_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_0_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_0_MC_Q_tsimrenamed_net_Q
    );
  Gnd : X_ZERO
    port map (
      O => Gnd_65
    );
  Vcc : X_ONE
    port map (
      O => Vcc_66
    );
  cnt_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D_IN1,
      O => cnt_0_MC_D_64
    );
  cnt_0_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D1_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D1_IN2,
      O => cnt_0_MC_D1_67
    );
  cnt_0_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2,
      O => cnt_0_MC_D2_PT_0_73
    );
  cnt_0_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2,
      O => cnt_0_MC_D2_PT_1_74
    );
  cnt_0_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_0_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_0_MC_D2_IN1,
      O => cnt_0_MC_D2_68
    );
  summand_v_mux0001_10_Q : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_Q_75,
      O => summand_v_mux0001(10)
    );
  summand_v_mux0001_10_MC_Q : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_D_76,
      O => summand_v_mux0001_10_MC_Q_75
    );
  summand_v_mux0001_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_summand_v_mux0001_10_MC_D_IN0,
      I1 => NlwBufferSignal_summand_v_mux0001_10_MC_D_IN1,
      O => summand_v_mux0001_10_MC_D_76
    );
  summand_v_mux0001_10_MC_D1 : X_ZERO
    port map (
      O => summand_v_mux0001_10_MC_D1_77
    );
  summand_v_mux0001_10_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2,
      O => summand_v_mux0001_10_MC_D2_PT_0_82
    );
  summand_v_mux0001_10_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN2,
      O => summand_v_mux0001_10_MC_D2_PT_1_84
    );
  summand_v_mux0001_10_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN0,
      I1 => NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN1,
      O => summand_v_mux0001_10_MC_D2_78
    );
  down_new_cs : X_BUF
    port map (
      I => down_new_cs_MC_Q,
      O => down_new_cs_79
    );
  down_new_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_down_new_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_down_new_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => down_new_cs_MC_Q
    );
  down_new_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_down_new_cs_MC_D_IN0,
      I1 => NlwBufferSignal_down_new_cs_MC_D_IN1,
      O => down_new_cs_MC_D_86
    );
  down_new_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_down_new_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_down_new_cs_MC_D1_IN1,
      O => down_new_cs_MC_D1_87
    );
  down_new_cs_MC_D2 : X_ZERO
    port map (
      O => down_new_cs_MC_D2_88
    );
  up_new_cs : X_BUF
    port map (
      I => up_new_cs_MC_Q,
      O => up_new_cs_80
    );
  up_new_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_up_new_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_up_new_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => up_new_cs_MC_Q
    );
  up_new_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_up_new_cs_MC_D_IN0,
      I1 => NlwBufferSignal_up_new_cs_MC_D_IN1,
      O => up_new_cs_MC_D_90
    );
  up_new_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_up_new_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_up_new_cs_MC_D1_IN1,
      O => up_new_cs_MC_D1_91
    );
  up_new_cs_MC_D2 : X_ZERO
    port map (
      O => up_new_cs_MC_D2_92
    );
  down_old_cs : X_BUF
    port map (
      I => down_old_cs_MC_Q,
      O => down_old_cs_81
    );
  down_old_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_down_old_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_down_old_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => down_old_cs_MC_Q
    );
  down_old_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_down_old_cs_MC_D_IN0,
      I1 => NlwBufferSignal_down_old_cs_MC_D_IN1,
      O => down_old_cs_MC_D_94
    );
  down_old_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_down_old_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_down_old_cs_MC_D1_IN1,
      O => down_old_cs_MC_D1_95
    );
  down_old_cs_MC_D2 : X_ZERO
    port map (
      O => down_old_cs_MC_D2_96
    );
  up_old_cs : X_BUF
    port map (
      I => up_old_cs_MC_Q,
      O => up_old_cs_83
    );
  up_old_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_up_old_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_up_old_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => up_old_cs_MC_Q
    );
  up_old_cs_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_up_old_cs_MC_D_IN0,
      I1 => NlwBufferSignal_up_old_cs_MC_D_IN1,
      O => up_old_cs_MC_D_98
    );
  up_old_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_up_old_cs_MC_D1_IN0,
      I1 => NlwBufferSignal_up_old_cs_MC_D1_IN1,
      O => up_old_cs_MC_D1_99
    );
  up_old_cs_MC_D2 : X_ZERO
    port map (
      O => up_old_cs_MC_D2_100
    );
  N_PZ_137 : X_BUF
    port map (
      I => N_PZ_137_MC_Q_101,
      O => N_PZ_137_70
    );
  N_PZ_137_MC_Q : X_BUF
    port map (
      I => N_PZ_137_MC_D_102,
      O => N_PZ_137_MC_Q_101
    );
  N_PZ_137_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_137_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_137_MC_D_IN1,
      O => N_PZ_137_MC_D_102
    );
  N_PZ_137_MC_D1 : X_ZERO
    port map (
      O => N_PZ_137_MC_D1_103
    );
  N_PZ_137_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN2,
      O => N_PZ_137_MC_D2_PT_0_105
    );
  N_PZ_137_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_137_MC_D2_PT_1_IN2,
      O => N_PZ_137_MC_D2_PT_1_106
    );
  N_PZ_137_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_137_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_137_MC_D2_IN1,
      O => N_PZ_137_MC_D2_104
    );
  valLd_cs_0_Q : X_BUF
    port map (
      I => valLd_cs_0_MC_Q,
      O => valLd_cs(0)
    );
  valLd_cs_0_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_0_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_0_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_0_MC_Q
    );
  valLd_cs_0_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D_IN1,
      O => valLd_cs_0_MC_D_108
    );
  valLd_cs_0_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_0_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_0_MC_D1_IN1,
      O => valLd_cs_0_MC_D1_109
    );
  valLd_cs_0_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_0_MC_D2_110
    );
  N_PZ_133 : X_BUF
    port map (
      I => N_PZ_133_MC_Q_111,
      O => N_PZ_133_72
    );
  N_PZ_133_MC_Q : X_BUF
    port map (
      I => N_PZ_133_MC_D_112,
      O => N_PZ_133_MC_Q_111
    );
  N_PZ_133_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_133_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_133_MC_D_IN1,
      O => N_PZ_133_MC_D_112
    );
  N_PZ_133_MC_D1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_133_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_133_MC_D1_IN1,
      O => N_PZ_133_MC_D1_113
    );
  N_PZ_133_MC_D2 : X_ZERO
    port map (
      O => N_PZ_133_MC_D2_114
    );
  nLd_new_cs : X_BUF
    port map (
      I => nLd_new_cs_MC_Q,
      O => nLd_new_cs_115
    );
  nLd_new_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nLd_new_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_nLd_new_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => nLd_new_cs_MC_Q
    );
  nLd_new_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_nLd_new_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nLd_new_cs_MC_D_IN1,
      O => nLd_new_cs_MC_D_118
    );
  nLd_new_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nLd_new_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_nLd_new_cs_MC_D1_IN1,
      O => nLd_new_cs_MC_D1_119
    );
  nLd_new_cs_MC_D2 : X_ZERO
    port map (
      O => nLd_new_cs_MC_D2_120
    );
  nLd_old_cs : X_BUF
    port map (
      I => nLd_old_cs_MC_Q,
      O => nLd_old_cs_116
    );
  nLd_old_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nLd_old_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_nLd_old_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => nLd_old_cs_MC_Q
    );
  nLd_old_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_nLd_old_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nLd_old_cs_MC_D_IN1,
      O => nLd_old_cs_MC_D_122
    );
  nLd_old_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nLd_old_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_nLd_old_cs_MC_D1_IN1,
      O => nLd_old_cs_MC_D1_123
    );
  nLd_old_cs_MC_D2 : X_ZERO
    port map (
      O => nLd_old_cs_MC_D2_124
    );
  cnt_10_MC_Q : X_BUF
    port map (
      I => cnt_10_MC_Q_tsimrenamed_net_Q_125,
      O => cnt_10_MC_Q_39
    );
  cnt_10_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_10_MC_D_126,
      O => cnt_10_MC_Q_tsimrenamed_net_Q_125
    );
  cnt_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D_IN1,
      O => cnt_10_MC_D_126
    );
  cnt_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_10_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_10_MC_D1_IN1,
      O => cnt_10_MC_D1_127
    );
  cnt_10_MC_D2 : X_ZERO
    port map (
      O => cnt_10_MC_D2_128
    );
  cnt_10_BUFR : X_BUF
    port map (
      I => cnt_10_BUFR_MC_Q,
      O => cnt_10_BUFR_129
    );
  cnt_10_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_10_BUFR_MC_Q
    );
  cnt_10_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D_IN1,
      O => cnt_10_BUFR_MC_D_131
    );
  cnt_10_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_10_BUFR_MC_D1_132
    );
  cnt_10_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1,
      O => cnt_10_BUFR_MC_D2_PT_0_135
    );
  cnt_10_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3,
      O => cnt_10_BUFR_MC_D2_PT_1_138
    );
  cnt_10_BUFR_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN3,
      O => cnt_10_BUFR_MC_D2_PT_2_139
    );
  cnt_10_BUFR_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2,
      O => cnt_10_BUFR_MC_D2_133
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_Q_140,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_Q : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_141,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_Q_140
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN1,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_141
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D1 : X_ZERO
    port map (
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D1_142
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN3,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_146
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1 : X_AND7
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN5,
      I6 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN6,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_152
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2 : X_AND7
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN5,
      I6 => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_154
    );
  Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN2,
      O => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_143
    );
  cnt_ns_9_Q : X_BUF
    port map (
      I => cnt_ns_9_MC_Q_155,
      O => cnt_ns_9_Q_147
    );
  cnt_ns_9_MC_Q : X_BUF
    port map (
      I => cnt_ns_9_MC_D_156,
      O => cnt_ns_9_MC_Q_155
    );
  cnt_ns_9_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_cnt_ns_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D_IN1,
      O => cnt_ns_9_MC_D_156
    );
  cnt_ns_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_ns_9_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D1_IN1,
      O => cnt_ns_9_MC_D1_157
    );
  cnt_ns_9_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN4,
      O => cnt_ns_9_MC_D2_PT_0_159
    );
  cnt_ns_9_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN5,
      O => cnt_ns_9_MC_D2_PT_1_161
    );
  cnt_ns_9_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN5,
      O => cnt_ns_9_MC_D2_PT_2_162
    );
  cnt_ns_9_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN5,
      O => cnt_ns_9_MC_D2_PT_3_163
    );
  cnt_ns_9_MC_D2_PT_4 : X_AND6
    port map (
      I0 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN5,
      O => cnt_ns_9_MC_D2_PT_4_164
    );
  cnt_ns_9_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_cnt_ns_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_ns_9_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_ns_9_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_ns_9_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_ns_9_MC_D2_IN4,
      O => cnt_ns_9_MC_D2_158
    );
  cnt_v_mux0000_9_Q : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_Q_165,
      O => cnt_v_mux0000_9_Q_144
    );
  cnt_v_mux0000_9_MC_Q : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_D_166,
      O => cnt_v_mux0000_9_MC_Q_165
    );
  cnt_v_mux0000_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN1,
      O => cnt_v_mux0000_9_MC_D_166
    );
  cnt_v_mux0000_9_MC_D1 : X_ZERO
    port map (
      O => cnt_v_mux0000_9_MC_D1_167
    );
  cnt_v_mux0000_9_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1,
      O => cnt_v_mux0000_9_MC_D2_PT_0_170
    );
  cnt_v_mux0000_9_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1,
      O => cnt_v_mux0000_9_MC_D2_PT_1_172
    );
  cnt_v_mux0000_9_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN1,
      O => cnt_v_mux0000_9_MC_D2_168
    );
  valLd_cs_9_Q : X_BUF
    port map (
      I => valLd_cs_9_MC_Q,
      O => valLd_cs(9)
    );
  valLd_cs_9_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_9_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_9_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_9_MC_Q
    );
  valLd_cs_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D_IN1,
      O => valLd_cs_9_MC_D_174
    );
  valLd_cs_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_9_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_9_MC_D1_IN1,
      O => valLd_cs_9_MC_D1_175
    );
  valLd_cs_9_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_9_MC_D2_176
    );
  cnt_9_BUFR : X_BUF
    port map (
      I => cnt_9_BUFR_MC_Q,
      O => cnt_9_BUFR_171
    );
  cnt_9_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_9_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_9_BUFR_MC_Q
    );
  cnt_9_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D_IN1,
      O => cnt_9_BUFR_MC_D_178
    );
  cnt_9_BUFR_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_9_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_9_BUFR_MC_D1_IN1,
      O => cnt_9_BUFR_MC_D1_179
    );
  cnt_9_BUFR_MC_D2 : X_ZERO
    port map (
      O => cnt_9_BUFR_MC_D2_180
    );
  cnt_v_mux0000_5_Q : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_Q_181,
      O => cnt_v_mux0000_5_Q_148
    );
  cnt_v_mux0000_5_MC_Q : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_D_182,
      O => cnt_v_mux0000_5_MC_Q_181
    );
  cnt_v_mux0000_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN1,
      O => cnt_v_mux0000_5_MC_D_182
    );
  cnt_v_mux0000_5_MC_D1 : X_ZERO
    port map (
      O => cnt_v_mux0000_5_MC_D1_183
    );
  cnt_v_mux0000_5_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1,
      O => cnt_v_mux0000_5_MC_D2_PT_0_186
    );
  cnt_v_mux0000_5_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1,
      O => cnt_v_mux0000_5_MC_D2_PT_1_188
    );
  cnt_v_mux0000_5_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN1,
      O => cnt_v_mux0000_5_MC_D2_184
    );
  valLd_cs_5_Q : X_BUF
    port map (
      I => valLd_cs_5_MC_Q,
      O => valLd_cs(5)
    );
  valLd_cs_5_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_5_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_5_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_5_MC_Q
    );
  valLd_cs_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D_IN1,
      O => valLd_cs_5_MC_D_190
    );
  valLd_cs_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_5_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_5_MC_D1_IN1,
      O => valLd_cs_5_MC_D1_191
    );
  valLd_cs_5_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_5_MC_D2_192
    );
  cnt_5_BUFR : X_BUF
    port map (
      I => cnt_5_BUFR_MC_Q,
      O => cnt_5_BUFR_187
    );
  cnt_5_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_5_BUFR_MC_Q
    );
  cnt_5_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D_IN1,
      O => cnt_5_BUFR_MC_D_194
    );
  cnt_5_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_5_BUFR_MC_D1_195
    );
  cnt_5_BUFR_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2,
      O => cnt_5_BUFR_MC_D2_PT_0_198
    );
  cnt_5_BUFR_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN2,
      O => cnt_5_BUFR_MC_D2_PT_1_199
    );
  cnt_5_BUFR_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1,
      O => cnt_5_BUFR_MC_D2_196
    );
  N_PZ_138 : X_BUF
    port map (
      I => N_PZ_138_MC_Q_200,
      O => N_PZ_138_197
    );
  N_PZ_138_MC_Q : X_BUF
    port map (
      I => N_PZ_138_MC_D_201,
      O => N_PZ_138_MC_Q_200
    );
  N_PZ_138_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_138_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_138_MC_D_IN1,
      O => N_PZ_138_MC_D_201
    );
  N_PZ_138_MC_D1 : X_ZERO
    port map (
      O => N_PZ_138_MC_D1_202
    );
  N_PZ_138_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_138_MC_D2_PT_0_IN2,
      O => N_PZ_138_MC_D2_PT_0_206
    );
  N_PZ_138_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_138_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN2,
      O => N_PZ_138_MC_D2_PT_1_208
    );
  N_PZ_138_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_138_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_138_MC_D2_IN1,
      O => N_PZ_138_MC_D2_203
    );
  err_cs_and000072 : X_BUF
    port map (
      I => err_cs_and000072_MC_Q_209,
      O => err_cs_and000072_207
    );
  err_cs_and000072_MC_Q : X_BUF
    port map (
      I => err_cs_and000072_MC_D_210,
      O => err_cs_and000072_MC_Q_209
    );
  err_cs_and000072_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_cs_and000072_MC_D_IN0,
      I1 => NlwBufferSignal_err_cs_and000072_MC_D_IN1,
      O => err_cs_and000072_MC_D_210
    );
  err_cs_and000072_MC_D1 : X_ZERO
    port map (
      O => err_cs_and000072_MC_D1_211
    );
  err_cs_and000072_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_cs_and000072_MC_D2_PT_0_IN1,
      O => err_cs_and000072_MC_D2_PT_0_213
    );
  err_cs_and000072_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN3,
      O => err_cs_and000072_MC_D2_PT_1_216
    );
  err_cs_and000072_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN5,
      O => err_cs_and000072_MC_D2_PT_2_220
    );
  err_cs_and000072_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_err_cs_and000072_MC_D2_IN0,
      I1 => NlwBufferSignal_err_cs_and000072_MC_D2_IN1,
      I2 => NlwBufferSignal_err_cs_and000072_MC_D2_IN2,
      O => err_cs_and000072_MC_D2_212
    );
  SF4 : X_BUF
    port map (
      I => SF4_MC_Q_221,
      O => SF4_204
    );
  SF4_MC_Q : X_BUF
    port map (
      I => SF4_MC_D_222,
      O => SF4_MC_Q_221
    );
  SF4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_SF4_MC_D_IN0,
      I1 => NlwBufferSignal_SF4_MC_D_IN1,
      O => SF4_MC_D_222
    );
  SF4_MC_D1 : X_ZERO
    port map (
      O => SF4_MC_D1_223
    );
  SF4_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwInverterSignal_SF4_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_SF4_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_SF4_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_SF4_MC_D2_PT_0_IN3,
      O => SF4_MC_D2_PT_0_226
    );
  SF4_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_SF4_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_SF4_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_SF4_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_SF4_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_SF4_MC_D2_PT_1_IN4,
      I5 => NlwInverterSignal_SF4_MC_D2_PT_1_IN5,
      O => SF4_MC_D2_PT_1_227
    );
  SF4_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_SF4_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_SF4_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_SF4_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_SF4_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_SF4_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_SF4_MC_D2_PT_2_IN5,
      O => SF4_MC_D2_PT_2_229
    );
  SF4_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_SF4_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_SF4_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_SF4_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_SF4_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_SF4_MC_D2_PT_3_IN4,
      I5 => NlwInverterSignal_SF4_MC_D2_PT_3_IN5,
      O => SF4_MC_D2_PT_3_230
    );
  SF4_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_SF4_MC_D2_IN0,
      I1 => NlwBufferSignal_SF4_MC_D2_IN1,
      I2 => NlwBufferSignal_SF4_MC_D2_IN2,
      I3 => NlwBufferSignal_SF4_MC_D2_IN3,
      O => SF4_MC_D2_224
    );
  valLd_cs_1_Q : X_BUF
    port map (
      I => valLd_cs_1_MC_Q,
      O => valLd_cs(1)
    );
  valLd_cs_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_1_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_1_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_1_MC_Q
    );
  valLd_cs_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D_IN1,
      O => valLd_cs_1_MC_D_232
    );
  valLd_cs_1_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_1_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_1_MC_D1_IN1,
      O => valLd_cs_1_MC_D1_233
    );
  valLd_cs_1_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_1_MC_D2_234
    );
  valLd_cs_2_Q : X_BUF
    port map (
      I => valLd_cs_2_MC_Q,
      O => valLd_cs(2)
    );
  valLd_cs_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_2_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_2_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_2_MC_Q
    );
  valLd_cs_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D_IN1,
      O => valLd_cs_2_MC_D_236
    );
  valLd_cs_2_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_2_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_2_MC_D1_IN1,
      O => valLd_cs_2_MC_D1_237
    );
  valLd_cs_2_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_2_MC_D2_238
    );
  valLd_cs_3_Q : X_BUF
    port map (
      I => valLd_cs_3_MC_Q,
      O => valLd_cs(3)
    );
  valLd_cs_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_3_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_3_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_3_MC_Q
    );
  valLd_cs_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D_IN1,
      O => valLd_cs_3_MC_D_240
    );
  valLd_cs_3_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_3_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_3_MC_D1_IN1,
      O => valLd_cs_3_MC_D1_241
    );
  valLd_cs_3_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_3_MC_D2_242
    );
  cnt_1_MC_Q : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_Q_43
    );
  cnt_1_MC_UIM : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => cnt_1_MC_UIM_228
    );
  cnt_1_MC_tsimcreated_xor_Q : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1,
      O => cnt_1_MC_tsimcreated_xor_Q_245
    );
  cnt_1_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_1_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_1_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_1_MC_Q_tsimrenamed_net_Q
    );
  cnt_1_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D_IN1,
      O => cnt_1_MC_D_244
    );
  cnt_1_MC_D1 : X_ZERO
    port map (
      O => cnt_1_MC_D1_246
    );
  cnt_1_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1,
      O => cnt_1_MC_D2_PT_0_248
    );
  cnt_1_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2,
      O => cnt_1_MC_D2_PT_1_250
    );
  cnt_1_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2,
      O => cnt_1_MC_D2_PT_2_251
    );
  cnt_1_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2,
      O => cnt_1_MC_D2_PT_3_252
    );
  cnt_1_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3,
      O => cnt_1_MC_D2_PT_4_253
    );
  cnt_1_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4,
      O => cnt_1_MC_D2_PT_5_254
    );
  cnt_1_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4,
      O => cnt_1_MC_D2_PT_6_255
    );
  cnt_1_MC_D2_PT_7 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_1_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN4,
      I5 => NlwInverterSignal_cnt_1_MC_D2_PT_7_IN5,
      O => cnt_1_MC_D2_PT_7_256
    );
  cnt_1_MC_D2 : X_OR8
    port map (
      I0 => NlwBufferSignal_cnt_1_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_1_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_1_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_1_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_1_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_1_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_1_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_1_MC_D2_IN7,
      O => cnt_1_MC_D2_247
    );
  N_PZ_232 : X_BUF
    port map (
      I => N_PZ_232_MC_Q_257,
      O => N_PZ_232_249
    );
  N_PZ_232_MC_Q : X_BUF
    port map (
      I => N_PZ_232_MC_D_258,
      O => N_PZ_232_MC_Q_257
    );
  N_PZ_232_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_232_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_232_MC_D_IN1,
      O => N_PZ_232_MC_D_258
    );
  N_PZ_232_MC_D1 : X_ZERO
    port map (
      O => N_PZ_232_MC_D1_259
    );
  N_PZ_232_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_232_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN2,
      O => N_PZ_232_MC_D2_PT_0_261
    );
  N_PZ_232_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_232_MC_D2_PT_1_IN3,
      O => N_PZ_232_MC_D2_PT_1_262
    );
  N_PZ_232_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_232_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_232_MC_D2_IN1,
      O => N_PZ_232_MC_D2_260
    );
  cnt_2_MC_Q : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_Q_45
    );
  cnt_2_MC_UIM : X_BUF
    port map (
      I => cnt_2_MC_Q_tsimrenamed_net_Q,
      O => cnt_2_MC_UIM_214
    );
  cnt_2_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_2_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_2_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_2_MC_Q_tsimrenamed_net_Q
    );
  cnt_2_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D_IN1,
      O => cnt_2_MC_D_264
    );
  cnt_2_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D1_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D1_IN2,
      O => cnt_2_MC_D1_265
    );
  cnt_2_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN4,
      O => cnt_2_MC_D2_PT_0_267
    );
  cnt_2_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4,
      O => cnt_2_MC_D2_PT_1_268
    );
  cnt_2_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4,
      O => cnt_2_MC_D2_PT_2_269
    );
  cnt_2_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4,
      O => cnt_2_MC_D2_PT_3_270
    );
  cnt_2_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4,
      O => cnt_2_MC_D2_PT_4_271
    );
  cnt_2_MC_D2_PT_5 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN5,
      O => cnt_2_MC_D2_PT_5_272
    );
  cnt_2_MC_D2_PT_6 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5,
      O => cnt_2_MC_D2_PT_6_273
    );
  cnt_2_MC_D2 : X_OR7
    port map (
      I0 => NlwBufferSignal_cnt_2_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_2_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_2_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_2_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_2_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_2_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_2_MC_D2_IN6,
      O => cnt_2_MC_D2_266
    );
  N_PZ_294 : X_BUF
    port map (
      I => N_PZ_294_MC_Q_274,
      O => N_PZ_294_215
    );
  N_PZ_294_MC_Q : X_BUF
    port map (
      I => N_PZ_294_MC_D_275,
      O => N_PZ_294_MC_Q_274
    );
  N_PZ_294_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_294_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_294_MC_D_IN1,
      O => N_PZ_294_MC_D_275
    );
  N_PZ_294_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_294_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_294_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_294_MC_D1_IN2,
      O => N_PZ_294_MC_D1_276
    );
  N_PZ_294_MC_D2 : X_ZERO
    port map (
      O => N_PZ_294_MC_D2_277
    );
  cnt_3_MC_Q : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_Q_47
    );
  cnt_3_MC_UIM : X_BUF
    port map (
      I => cnt_3_MC_Q_tsimrenamed_net_Q,
      O => cnt_3_MC_UIM_225
    );
  cnt_3_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_3_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_3_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_3_MC_Q_tsimrenamed_net_Q
    );
  cnt_3_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D_IN1,
      O => cnt_3_MC_D_279
    );
  cnt_3_MC_D1 : X_ZERO
    port map (
      O => cnt_3_MC_D1_280
    );
  cnt_3_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1,
      O => cnt_3_MC_D2_PT_0_282
    );
  cnt_3_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3,
      O => cnt_3_MC_D2_PT_1_283
    );
  cnt_3_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4,
      O => cnt_3_MC_D2_PT_2_284
    );
  cnt_3_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4,
      O => cnt_3_MC_D2_PT_3_285
    );
  cnt_3_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4,
      O => cnt_3_MC_D2_PT_4_286
    );
  cnt_3_MC_D2_PT_5 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN4,
      O => cnt_3_MC_D2_PT_5_287
    );
  cnt_3_MC_D2_PT_6 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_6_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN4,
      O => cnt_3_MC_D2_PT_6_288
    );
  cnt_3_MC_D2_PT_7 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN2,
      I3 => NlwInverterSignal_cnt_3_MC_D2_PT_7_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN4,
      O => cnt_3_MC_D2_PT_7_289
    );
  cnt_3_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_cnt_3_MC_D2_PT_8_IN1,
      I2 => NlwInverterSignal_cnt_3_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN4,
      O => cnt_3_MC_D2_PT_8_290
    );
  cnt_3_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_cnt_3_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_3_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_3_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_3_MC_D2_IN3,
      I4 => NlwBufferSignal_cnt_3_MC_D2_IN4,
      I5 => NlwBufferSignal_cnt_3_MC_D2_IN5,
      I6 => NlwBufferSignal_cnt_3_MC_D2_IN6,
      I7 => NlwBufferSignal_cnt_3_MC_D2_IN7,
      I8 => NlwBufferSignal_cnt_3_MC_D2_IN8,
      I9 => NlwBufferSignal_cnt_3_MC_D2_IN9,
      I10 => NlwBufferSignal_cnt_3_MC_D2_IN10,
      I11 => NlwBufferSignal_cnt_3_MC_D2_IN11,
      I12 => NlwBufferSignal_cnt_3_MC_D2_IN12,
      I13 => NlwBufferSignal_cnt_3_MC_D2_IN13,
      I14 => NlwBufferSignal_cnt_3_MC_D2_IN14,
      I15 => NlwBufferSignal_cnt_3_MC_D2_IN15,
      O => cnt_3_MC_D2_281
    );
  N_PZ_135 : X_BUF
    port map (
      I => N_PZ_135_MC_Q_291,
      O => N_PZ_135_205
    );
  N_PZ_135_MC_Q : X_BUF
    port map (
      I => N_PZ_135_MC_D_292,
      O => N_PZ_135_MC_Q_291
    );
  N_PZ_135_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_135_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_135_MC_D_IN1,
      O => N_PZ_135_MC_D_292
    );
  N_PZ_135_MC_D1 : X_ZERO
    port map (
      O => N_PZ_135_MC_D1_293
    );
  N_PZ_135_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN1,
      O => N_PZ_135_MC_D2_PT_0_296
    );
  N_PZ_135_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_135_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN1,
      O => N_PZ_135_MC_D2_PT_1_298
    );
  N_PZ_135_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_135_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_135_MC_D2_IN1,
      O => N_PZ_135_MC_D2_294
    );
  valLd_cs_4_Q : X_BUF
    port map (
      I => valLd_cs_4_MC_Q,
      O => valLd_cs(4)
    );
  valLd_cs_4_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_4_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_4_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_4_MC_Q
    );
  valLd_cs_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D_IN1,
      O => valLd_cs_4_MC_D_300
    );
  valLd_cs_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_4_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_4_MC_D1_IN1,
      O => valLd_cs_4_MC_D1_301
    );
  valLd_cs_4_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_4_MC_D2_302
    );
  cnt_4_BUFR : X_BUF
    port map (
      I => cnt_4_BUFR_MC_Q,
      O => cnt_4_BUFR_297
    );
  cnt_4_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_4_BUFR_MC_Q
    );
  cnt_4_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D_IN1,
      O => cnt_4_BUFR_MC_D_304
    );
  cnt_4_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_4_BUFR_MC_D1_305
    );
  cnt_4_BUFR_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN2,
      O => cnt_4_BUFR_MC_D2_PT_0_307
    );
  cnt_4_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3,
      O => cnt_4_BUFR_MC_D2_PT_1_308
    );
  cnt_4_BUFR_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3,
      O => cnt_4_BUFR_MC_D2_PT_2_309
    );
  cnt_4_BUFR_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3,
      O => cnt_4_BUFR_MC_D2_PT_3_310
    );
  cnt_4_BUFR_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3,
      O => cnt_4_BUFR_MC_D2_306
    );
  SF87 : X_BUF
    port map (
      I => SF87_MC_Q_311,
      O => SF87_160
    );
  SF87_MC_Q : X_BUF
    port map (
      I => SF87_MC_D_312,
      O => SF87_MC_Q_311
    );
  SF87_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_SF87_MC_D_IN0,
      I1 => NlwBufferSignal_SF87_MC_D_IN1,
      O => SF87_MC_D_312
    );
  SF87_MC_D1 : X_ZERO
    port map (
      O => SF87_MC_D1_313
    );
  SF87_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_SF87_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_SF87_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_SF87_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_SF87_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_SF87_MC_D2_PT_0_IN4,
      O => SF87_MC_D2_PT_0_315
    );
  SF87_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwInverterSignal_SF87_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_SF87_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_SF87_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_SF87_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_SF87_MC_D2_PT_1_IN4,
      O => SF87_MC_D2_PT_1_316
    );
  SF87_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_SF87_MC_D2_IN0,
      I1 => NlwBufferSignal_SF87_MC_D2_IN1,
      O => SF87_MC_D2_314
    );
  N_PZ_139 : X_BUF
    port map (
      I => N_PZ_139_MC_Q_317,
      O => N_PZ_139_149
    );
  N_PZ_139_MC_Q : X_BUF
    port map (
      I => N_PZ_139_MC_D_318,
      O => N_PZ_139_MC_Q_317
    );
  N_PZ_139_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D_IN1,
      O => N_PZ_139_MC_D_318
    );
  N_PZ_139_MC_D1 : X_ZERO
    port map (
      O => N_PZ_139_MC_D1_319
    );
  N_PZ_139_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1,
      O => N_PZ_139_MC_D2_PT_0_322
    );
  N_PZ_139_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_139_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1,
      O => N_PZ_139_MC_D2_PT_1_324
    );
  N_PZ_139_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_139_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_139_MC_D2_IN1,
      O => N_PZ_139_MC_D2_320
    );
  valLd_cs_6_Q : X_BUF
    port map (
      I => valLd_cs_6_MC_Q,
      O => valLd_cs(6)
    );
  valLd_cs_6_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_6_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_6_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_6_MC_Q
    );
  valLd_cs_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D_IN1,
      O => valLd_cs_6_MC_D_326
    );
  valLd_cs_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_6_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_6_MC_D1_IN1,
      O => valLd_cs_6_MC_D1_327
    );
  valLd_cs_6_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_6_MC_D2_328
    );
  cnt_6_BUFR : X_BUF
    port map (
      I => cnt_6_BUFR_MC_Q,
      O => cnt_6_BUFR_323
    );
  cnt_6_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_6_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_6_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_6_BUFR_MC_Q
    );
  cnt_6_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_BUFR_MC_D_IN1,
      O => cnt_6_BUFR_MC_D_330
    );
  cnt_6_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_6_BUFR_MC_D1_331
    );
  cnt_6_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN1,
      O => cnt_6_BUFR_MC_D2_PT_0_333
    );
  cnt_6_BUFR_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_6_BUFR_MC_D2_PT_1_IN2,
      O => cnt_6_BUFR_MC_D2_PT_1_334
    );
  cnt_6_BUFR_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_6_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_6_BUFR_MC_D2_IN1,
      O => cnt_6_BUFR_MC_D2_332
    );
  N_PZ_253 : X_BUF
    port map (
      I => N_PZ_253_MC_Q_335,
      O => N_PZ_253_150
    );
  N_PZ_253_MC_Q : X_BUF
    port map (
      I => N_PZ_253_MC_D_336,
      O => N_PZ_253_MC_Q_335
    );
  N_PZ_253_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_253_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_253_MC_D_IN1,
      O => N_PZ_253_MC_D_336
    );
  N_PZ_253_MC_D1 : X_ZERO
    port map (
      O => N_PZ_253_MC_D1_337
    );
  N_PZ_253_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN2,
      O => N_PZ_253_MC_D2_PT_0_339
    );
  N_PZ_253_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN2,
      O => N_PZ_253_MC_D2_PT_1_340
    );
  N_PZ_253_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_253_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_253_MC_D2_IN1,
      O => N_PZ_253_MC_D2_338
    );
  cnt_v_mux0000_8_Q : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_Q_341,
      O => cnt_v_mux0000_8_Q_151
    );
  cnt_v_mux0000_8_MC_Q : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_D_342,
      O => cnt_v_mux0000_8_MC_Q_341
    );
  cnt_v_mux0000_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN1,
      O => cnt_v_mux0000_8_MC_D_342
    );
  cnt_v_mux0000_8_MC_D1 : X_ZERO
    port map (
      O => cnt_v_mux0000_8_MC_D1_343
    );
  cnt_v_mux0000_8_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1,
      O => cnt_v_mux0000_8_MC_D2_PT_0_346
    );
  cnt_v_mux0000_8_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1,
      O => cnt_v_mux0000_8_MC_D2_PT_1_348
    );
  cnt_v_mux0000_8_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN1,
      O => cnt_v_mux0000_8_MC_D2_344
    );
  valLd_cs_8_Q : X_BUF
    port map (
      I => valLd_cs_8_MC_Q,
      O => valLd_cs(8)
    );
  valLd_cs_8_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_8_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_8_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_8_MC_Q
    );
  valLd_cs_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D_IN1,
      O => valLd_cs_8_MC_D_350
    );
  valLd_cs_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_8_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_8_MC_D1_IN1,
      O => valLd_cs_8_MC_D1_351
    );
  valLd_cs_8_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_8_MC_D2_352
    );
  cnt_8_BUFR : X_BUF
    port map (
      I => cnt_8_BUFR_MC_Q,
      O => cnt_8_BUFR_347
    );
  cnt_8_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_8_BUFR_MC_Q
    );
  cnt_8_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1,
      O => cnt_8_BUFR_MC_D_354
    );
  cnt_8_BUFR_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D1_IN1,
      O => cnt_8_BUFR_MC_D1_355
    );
  cnt_8_BUFR_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3,
      I4 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4,
      O => cnt_8_BUFR_MC_D2_PT_0_358
    );
  cnt_8_BUFR_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4,
      O => cnt_8_BUFR_MC_D2_PT_1_359
    );
  cnt_8_BUFR_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN4,
      I5 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN5,
      O => cnt_8_BUFR_MC_D2_PT_2_360
    );
  cnt_8_BUFR_MC_D2_PT_3 : X_AND6
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3,
      I4 => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN5,
      O => cnt_8_BUFR_MC_D2_PT_3_361
    );
  cnt_8_BUFR_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3,
      O => cnt_8_BUFR_MC_D2_356
    );
  cnt_ns_7_Q : X_BUF
    port map (
      I => cnt_ns_7_MC_Q_362,
      O => cnt_ns_7_Q_357
    );
  cnt_ns_7_MC_Q : X_BUF
    port map (
      I => cnt_ns_7_MC_D_363,
      O => cnt_ns_7_MC_Q_362
    );
  cnt_ns_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_ns_7_MC_D_IN1,
      O => cnt_ns_7_MC_D_363
    );
  cnt_ns_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_ns_7_MC_D1_IN1,
      O => cnt_ns_7_MC_D1_364
    );
  cnt_ns_7_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN3,
      O => cnt_ns_7_MC_D2_PT_0_366
    );
  cnt_ns_7_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN3,
      O => cnt_ns_7_MC_D2_PT_1_367
    );
  cnt_ns_7_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_ns_7_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN3,
      O => cnt_ns_7_MC_D2_PT_2_368
    );
  cnt_ns_7_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN3,
      O => cnt_ns_7_MC_D2_PT_3_369
    );
  cnt_ns_7_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_cnt_ns_7_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_ns_7_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_ns_7_MC_D2_IN2,
      I3 => NlwBufferSignal_cnt_ns_7_MC_D2_IN3,
      O => cnt_ns_7_MC_D2_365
    );
  N_PZ_134 : X_BUF
    port map (
      I => N_PZ_134_MC_Q_370,
      O => N_PZ_134_153
    );
  N_PZ_134_MC_Q : X_BUF
    port map (
      I => N_PZ_134_MC_D_371,
      O => N_PZ_134_MC_Q_370
    );
  N_PZ_134_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_134_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_134_MC_D_IN1,
      O => N_PZ_134_MC_D_371
    );
  N_PZ_134_MC_D1 : X_ZERO
    port map (
      O => N_PZ_134_MC_D1_372
    );
  N_PZ_134_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN1,
      O => N_PZ_134_MC_D2_PT_0_375
    );
  N_PZ_134_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_134_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN1,
      O => N_PZ_134_MC_D2_PT_1_377
    );
  N_PZ_134_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_134_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_134_MC_D2_IN1,
      O => N_PZ_134_MC_D2_373
    );
  valLd_cs_7_Q : X_BUF
    port map (
      I => valLd_cs_7_MC_Q,
      O => valLd_cs(7)
    );
  valLd_cs_7_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_7_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_7_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_7_MC_Q
    );
  valLd_cs_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D_IN1,
      O => valLd_cs_7_MC_D_379
    );
  valLd_cs_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_7_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_7_MC_D1_IN1,
      O => valLd_cs_7_MC_D1_380
    );
  valLd_cs_7_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_7_MC_D2_381
    );
  cnt_7_BUFR : X_BUF
    port map (
      I => cnt_7_BUFR_MC_Q,
      O => cnt_7_BUFR_376
    );
  cnt_7_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_7_BUFR_MC_Q
    );
  cnt_7_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1,
      O => cnt_7_BUFR_MC_D_383
    );
  cnt_7_BUFR_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1,
      O => cnt_7_BUFR_MC_D1_384
    );
  cnt_7_BUFR_MC_D2 : X_ZERO
    port map (
      O => cnt_7_BUFR_MC_D2_385
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_Q_386,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_Q : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_387,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_Q_386
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN1,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_387
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D1 : X_ZERO
    port map (
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D1_388
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0 : X_AND5
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN2,
      I3 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3,
      I4 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_390
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_391
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2 : X_AND16
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN6,
      I7 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7,
      I8 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8,
      I9 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN9,
      I10 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN10,
      I11 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN11,
      I12 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN12,
      I13 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN13,
      I14 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN14,
      I15 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN15,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_392
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN2,
      I3 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN6,
      I7 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7,
      I8 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN15,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_393
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4 : X_AND16
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4,
      I5 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5,
      I6 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN6,
      I7 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN7,
      I8 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN8,
      I9 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN9,
      I10 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN10,
      I11 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN11,
      I12 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN12,
      I13 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN13,
      I14 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN14,
      I15 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN15,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_394
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5 : X_AND16
    port map (
      I0 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0,
      I1 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3,
      I4 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4,
      I5 => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5,
      I6 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN6,
      I7 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN7,
      I8 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN8,
      I9 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN9,
      I10 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN10,
      I11 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN11,
      I12 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN12,
      I13 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN13,
      I14 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN14,
      I15 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN15,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_395
    );
  Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2 : X_OR6
    port map (
      I0 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN0,
      I1 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN1,
      I2 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN2,
      I3 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN3,
      I4 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN4,
      I5 => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN5,
      O => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_389
    );
  valLd_cs_10_Q : X_BUF
    port map (
      I => valLd_cs_10_MC_Q,
      O => valLd_cs(10)
    );
  valLd_cs_10_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_10_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_10_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_10_MC_Q
    );
  valLd_cs_10_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D_IN1,
      O => valLd_cs_10_MC_D_397
    );
  valLd_cs_10_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_10_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_10_MC_D1_IN1,
      O => valLd_cs_10_MC_D1_398
    );
  valLd_cs_10_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_10_MC_D2_399
    );
  N_PZ_190 : X_BUF
    port map (
      I => N_PZ_190_MC_Q_400,
      O => N_PZ_190_145
    );
  N_PZ_190_MC_Q : X_BUF
    port map (
      I => N_PZ_190_MC_D_401,
      O => N_PZ_190_MC_Q_400
    );
  N_PZ_190_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_190_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_190_MC_D_IN1,
      O => N_PZ_190_MC_D_401
    );
  N_PZ_190_MC_D1 : X_ZERO
    port map (
      O => N_PZ_190_MC_D1_402
    );
  N_PZ_190_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN1,
      O => N_PZ_190_MC_D2_PT_0_404
    );
  N_PZ_190_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN3,
      I4 => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN5,
      O => N_PZ_190_MC_D2_PT_1_405
    );
  N_PZ_190_MC_D2_PT_2 : X_AND6
    port map (
      I0 => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN3,
      I4 => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN5,
      O => N_PZ_190_MC_D2_PT_2_406
    );
  N_PZ_190_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_190_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_190_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_190_MC_D2_IN2,
      O => N_PZ_190_MC_D2_403
    );
  cnt_11_MC_Q : X_BUF
    port map (
      I => cnt_11_MC_Q_tsimrenamed_net_Q_407,
      O => cnt_11_MC_Q_41
    );
  cnt_11_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_11_MC_D_408,
      O => cnt_11_MC_Q_tsimrenamed_net_Q_407
    );
  cnt_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D_IN1,
      O => cnt_11_MC_D_408
    );
  cnt_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_11_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_11_MC_D1_IN1,
      O => cnt_11_MC_D1_409
    );
  cnt_11_MC_D2 : X_ZERO
    port map (
      O => cnt_11_MC_D2_410
    );
  cnt_11_BUFR : X_BUF
    port map (
      I => cnt_11_BUFR_MC_Q,
      O => cnt_11_BUFR_411
    );
  cnt_11_BUFR_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_cnt_11_BUFR_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_cnt_11_BUFR_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => cnt_11_BUFR_MC_Q
    );
  cnt_11_BUFR_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_11_BUFR_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_11_BUFR_MC_D_IN1,
      O => cnt_11_BUFR_MC_D_413
    );
  cnt_11_BUFR_MC_D1 : X_ZERO
    port map (
      O => cnt_11_BUFR_MC_D1_414
    );
  cnt_11_BUFR_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN1,
      O => cnt_11_BUFR_MC_D2_PT_0_417
    );
  cnt_11_BUFR_MC_D2_PT_1 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN3,
      O => cnt_11_BUFR_MC_D2_PT_1_420
    );
  cnt_11_BUFR_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_cnt_11_BUFR_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN3,
      O => cnt_11_BUFR_MC_D2_PT_2_421
    );
  cnt_11_BUFR_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN0,
      I1 => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN1,
      I2 => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN2,
      O => cnt_11_BUFR_MC_D2_415
    );
  valLd_cs_11_Q : X_BUF
    port map (
      I => valLd_cs_11_MC_Q,
      O => valLd_cs(11)
    );
  valLd_cs_11_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_valLd_cs_11_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_valLd_cs_11_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => valLd_cs_11_MC_Q
    );
  valLd_cs_11_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D_IN1,
      O => valLd_cs_11_MC_D_423
    );
  valLd_cs_11_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_valLd_cs_11_MC_D1_IN0,
      I1 => NlwBufferSignal_valLd_cs_11_MC_D1_IN1,
      O => valLd_cs_11_MC_D1_424
    );
  valLd_cs_11_MC_D2 : X_ZERO
    port map (
      O => valLd_cs_11_MC_D2_425
    );
  N_PZ_155 : X_BUF
    port map (
      I => N_PZ_155_MC_Q_426,
      O => N_PZ_155_419
    );
  N_PZ_155_MC_Q : X_BUF
    port map (
      I => N_PZ_155_MC_D_427,
      O => N_PZ_155_MC_Q_426
    );
  N_PZ_155_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_155_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_155_MC_D_IN1,
      O => N_PZ_155_MC_D_427
    );
  N_PZ_155_MC_D1 : X_ZERO
    port map (
      O => N_PZ_155_MC_D1_428
    );
  N_PZ_155_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_155_MC_D2_PT_0_IN1,
      O => N_PZ_155_MC_D2_PT_0_430
    );
  N_PZ_155_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN1,
      O => N_PZ_155_MC_D2_PT_1_431
    );
  N_PZ_155_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_155_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_155_MC_D2_IN1,
      O => N_PZ_155_MC_D2_429
    );
  N_PZ_266 : X_BUF
    port map (
      I => N_PZ_266_MC_Q_432,
      O => N_PZ_266_416
    );
  N_PZ_266_MC_Q : X_BUF
    port map (
      I => N_PZ_266_MC_D_433,
      O => N_PZ_266_MC_Q_432
    );
  N_PZ_266_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_266_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_266_MC_D_IN1,
      O => N_PZ_266_MC_D_433
    );
  N_PZ_266_MC_D1 : X_ZERO
    port map (
      O => N_PZ_266_MC_D1_434
    );
  N_PZ_266_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN1,
      I2 => NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN2,
      O => N_PZ_266_MC_D2_PT_0_436
    );
  N_PZ_266_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN2,
      O => N_PZ_266_MC_D2_PT_1_437
    );
  N_PZ_266_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_266_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_266_MC_D2_IN1,
      O => N_PZ_266_MC_D2_435
    );
  cnt_4_MC_Q : X_BUF
    port map (
      I => cnt_4_MC_Q_tsimrenamed_net_Q_438,
      O => cnt_4_MC_Q_49
    );
  cnt_4_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_4_MC_D_439,
      O => cnt_4_MC_Q_tsimrenamed_net_Q_438
    );
  cnt_4_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D_IN1,
      O => cnt_4_MC_D_439
    );
  cnt_4_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_4_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_4_MC_D1_IN1,
      O => cnt_4_MC_D1_440
    );
  cnt_4_MC_D2 : X_ZERO
    port map (
      O => cnt_4_MC_D2_441
    );
  cnt_5_MC_Q : X_BUF
    port map (
      I => cnt_5_MC_Q_tsimrenamed_net_Q_442,
      O => cnt_5_MC_Q_51
    );
  cnt_5_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_5_MC_D_443,
      O => cnt_5_MC_Q_tsimrenamed_net_Q_442
    );
  cnt_5_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D_IN1,
      O => cnt_5_MC_D_443
    );
  cnt_5_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_5_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_5_MC_D1_IN1,
      O => cnt_5_MC_D1_444
    );
  cnt_5_MC_D2 : X_ZERO
    port map (
      O => cnt_5_MC_D2_445
    );
  cnt_6_MC_Q : X_BUF
    port map (
      I => cnt_6_MC_Q_tsimrenamed_net_Q_446,
      O => cnt_6_MC_Q_53
    );
  cnt_6_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_6_MC_D_447,
      O => cnt_6_MC_Q_tsimrenamed_net_Q_446
    );
  cnt_6_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D_IN1,
      O => cnt_6_MC_D_447
    );
  cnt_6_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_6_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_6_MC_D1_IN1,
      O => cnt_6_MC_D1_448
    );
  cnt_6_MC_D2 : X_ZERO
    port map (
      O => cnt_6_MC_D2_449
    );
  cnt_7_MC_Q : X_BUF
    port map (
      I => cnt_7_MC_Q_tsimrenamed_net_Q_450,
      O => cnt_7_MC_Q_55
    );
  cnt_7_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_7_MC_D_451,
      O => cnt_7_MC_Q_tsimrenamed_net_Q_450
    );
  cnt_7_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D_IN1,
      O => cnt_7_MC_D_451
    );
  cnt_7_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_7_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_7_MC_D1_IN1,
      O => cnt_7_MC_D1_452
    );
  cnt_7_MC_D2 : X_ZERO
    port map (
      O => cnt_7_MC_D2_453
    );
  cnt_8_MC_Q : X_BUF
    port map (
      I => cnt_8_MC_Q_tsimrenamed_net_Q_454,
      O => cnt_8_MC_Q_57
    );
  cnt_8_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_8_MC_D_455,
      O => cnt_8_MC_Q_tsimrenamed_net_Q_454
    );
  cnt_8_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D_IN1,
      O => cnt_8_MC_D_455
    );
  cnt_8_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_8_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_8_MC_D1_IN1,
      O => cnt_8_MC_D1_456
    );
  cnt_8_MC_D2 : X_ZERO
    port map (
      O => cnt_8_MC_D2_457
    );
  cnt_9_MC_Q : X_BUF
    port map (
      I => cnt_9_MC_Q_tsimrenamed_net_Q_458,
      O => cnt_9_MC_Q_59
    );
  cnt_9_MC_Q_tsimrenamed_net_Q : X_BUF
    port map (
      I => cnt_9_MC_D_459,
      O => cnt_9_MC_Q_tsimrenamed_net_Q_458
    );
  cnt_9_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D_IN1,
      O => cnt_9_MC_D_459
    );
  cnt_9_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_cnt_9_MC_D1_IN0,
      I1 => NlwBufferSignal_cnt_9_MC_D1_IN1,
      O => cnt_9_MC_D1_460
    );
  cnt_9_MC_D2 : X_ZERO
    port map (
      O => cnt_9_MC_D2_461
    );
  err_MC_Q : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_Q_61
    );
  err_MC_UIM : X_BUF
    port map (
      I => err_MC_Q_tsimrenamed_net_Q,
      O => err_MC_UIM_463
    );
  err_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_err_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_err_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => err_MC_Q_tsimrenamed_net_Q
    );
  err_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_MC_D_IN0,
      I1 => NlwBufferSignal_err_MC_D_IN1,
      O => err_MC_D_464
    );
  err_MC_D1 : X_ZERO
    port map (
      O => err_MC_D1_465
    );
  err_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_0_IN1,
      O => err_MC_D2_PT_0_468
    );
  err_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_1_IN1,
      O => err_MC_D2_PT_1_470
    );
  err_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_2_IN2,
      O => err_MC_D2_PT_2_472
    );
  err_MC_D2_PT_3 : X_AND3
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_3_IN2,
      O => err_MC_D2_PT_3_473
    );
  err_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_4_IN3,
      O => err_MC_D2_PT_4_475
    );
  err_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_5_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_5_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_5_IN3,
      O => err_MC_D2_PT_5_476
    );
  err_MC_D2_PT_6 : X_AND4
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_6_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_6_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_6_IN3,
      O => err_MC_D2_PT_6_477
    );
  err_MC_D2_PT_7 : X_AND4
    port map (
      I0 => NlwInverterSignal_err_MC_D2_PT_7_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_7_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_7_IN3,
      O => err_MC_D2_PT_7_478
    );
  err_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_8_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_8_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_8_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_8_IN4,
      O => err_MC_D2_PT_8_481
    );
  err_MC_D2_PT_9 : X_AND5
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_9_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_9_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_9_IN4,
      O => err_MC_D2_PT_9_482
    );
  err_MC_D2_PT_10 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_10_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_10_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_10_IN2,
      I3 => NlwBufferSignal_err_MC_D2_PT_10_IN3,
      I4 => NlwInverterSignal_err_MC_D2_PT_10_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_10_IN5,
      O => err_MC_D2_PT_10_483
    );
  err_MC_D2_PT_11 : X_AND7
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_11_IN0,
      I1 => NlwBufferSignal_err_MC_D2_PT_11_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_11_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_11_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_11_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_11_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_11_IN6,
      O => err_MC_D2_PT_11_484
    );
  err_MC_D2_PT_12 : X_AND7
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_12_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_12_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_12_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_12_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_12_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_12_IN5,
      I6 => NlwInverterSignal_err_MC_D2_PT_12_IN6,
      O => err_MC_D2_PT_12_485
    );
  err_MC_D2_PT_13 : X_AND8
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_13_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_13_IN1,
      I2 => NlwBufferSignal_err_MC_D2_PT_13_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_13_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_13_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_13_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_13_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_13_IN7,
      O => err_MC_D2_PT_13_486
    );
  err_MC_D2_PT_14 : X_AND8
    port map (
      I0 => NlwBufferSignal_err_MC_D2_PT_14_IN0,
      I1 => NlwInverterSignal_err_MC_D2_PT_14_IN1,
      I2 => NlwInverterSignal_err_MC_D2_PT_14_IN2,
      I3 => NlwInverterSignal_err_MC_D2_PT_14_IN3,
      I4 => NlwBufferSignal_err_MC_D2_PT_14_IN4,
      I5 => NlwInverterSignal_err_MC_D2_PT_14_IN5,
      I6 => NlwBufferSignal_err_MC_D2_PT_14_IN6,
      I7 => NlwInverterSignal_err_MC_D2_PT_14_IN7,
      O => err_MC_D2_PT_14_487
    );
  err_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_err_MC_D2_IN0,
      I1 => NlwBufferSignal_err_MC_D2_IN1,
      I2 => NlwBufferSignal_err_MC_D2_IN2,
      I3 => NlwBufferSignal_err_MC_D2_IN3,
      I4 => NlwBufferSignal_err_MC_D2_IN4,
      I5 => NlwBufferSignal_err_MC_D2_IN5,
      I6 => NlwBufferSignal_err_MC_D2_IN6,
      I7 => NlwBufferSignal_err_MC_D2_IN7,
      I8 => NlwBufferSignal_err_MC_D2_IN8,
      I9 => NlwBufferSignal_err_MC_D2_IN9,
      I10 => NlwBufferSignal_err_MC_D2_IN10,
      I11 => NlwBufferSignal_err_MC_D2_IN11,
      I12 => NlwBufferSignal_err_MC_D2_IN12,
      I13 => NlwBufferSignal_err_MC_D2_IN13,
      I14 => NlwBufferSignal_err_MC_D2_IN14,
      I15 => NlwBufferSignal_err_MC_D2_IN15,
      O => err_MC_D2_466
    );
  N_PZ_247 : X_BUF
    port map (
      I => N_PZ_247_MC_Q_488,
      O => N_PZ_247_467
    );
  N_PZ_247_MC_Q : X_BUF
    port map (
      I => N_PZ_247_MC_D_489,
      O => N_PZ_247_MC_Q_488
    );
  N_PZ_247_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D_IN1,
      O => N_PZ_247_MC_D_489
    );
  N_PZ_247_MC_D1 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D1_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D1_IN1,
      I2 => NlwBufferSignal_N_PZ_247_MC_D1_IN2,
      O => N_PZ_247_MC_D1_490
    );
  N_PZ_247_MC_D2_PT_0 : X_AND4
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN2,
      I3 => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN3,
      O => N_PZ_247_MC_D2_PT_0_492
    );
  N_PZ_247_MC_D2_PT_1 : X_AND6
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN1,
      I2 => NlwInverterSignal_N_PZ_247_MC_D2_PT_1_IN2,
      I3 => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN4,
      I5 => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN5,
      O => N_PZ_247_MC_D2_PT_1_493
    );
  N_PZ_247_MC_D2_PT_2 : X_AND8
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN2,
      I3 => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN4,
      I5 => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN5,
      I6 => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN6,
      I7 => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN7,
      O => N_PZ_247_MC_D2_PT_2_494
    );
  N_PZ_247_MC_D2_PT_3 : X_AND16
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN1,
      I2 => NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN4,
      I5 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN5,
      I6 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN6,
      I7 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN7,
      I8 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN8,
      I9 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN9,
      I10 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN10,
      I11 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN11,
      I12 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN12,
      I13 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN13,
      I14 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN14,
      I15 => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN15,
      O => N_PZ_247_MC_D2_PT_3_495
    );
  N_PZ_247_MC_D2 : X_OR4
    port map (
      I0 => NlwBufferSignal_N_PZ_247_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_247_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_247_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_247_MC_D2_IN3,
      O => N_PZ_247_MC_D2_491
    );
  err_cs_or0003 : X_BUF
    port map (
      I => err_cs_or0003_MC_Q_496,
      O => err_cs_or0003_471
    );
  err_cs_or0003_MC_Q : X_BUF
    port map (
      I => err_cs_or0003_MC_D_497,
      O => err_cs_or0003_MC_Q_496
    );
  err_cs_or0003_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D_IN1,
      O => err_cs_or0003_MC_D_497
    );
  err_cs_or0003_MC_D1 : X_ZERO
    port map (
      O => err_cs_or0003_MC_D1_498
    );
  err_cs_or0003_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN1,
      O => err_cs_or0003_MC_D2_PT_0_502
    );
  err_cs_or0003_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN2,
      O => err_cs_or0003_MC_D2_PT_1_503
    );
  err_cs_or0003_MC_D2_PT_2 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN3,
      O => err_cs_or0003_MC_D2_PT_2_505
    );
  err_cs_or0003_MC_D2_PT_3 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN3,
      O => err_cs_or0003_MC_D2_PT_3_506
    );
  err_cs_or0003_MC_D2_PT_4 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN3,
      O => err_cs_or0003_MC_D2_PT_4_507
    );
  err_cs_or0003_MC_D2_PT_5 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_5_IN3,
      O => err_cs_or0003_MC_D2_PT_5_508
    );
  err_cs_or0003_MC_D2_PT_6 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN3,
      O => err_cs_or0003_MC_D2_PT_6_509
    );
  err_cs_or0003_MC_D2_PT_7 : X_AND4
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN3,
      O => err_cs_or0003_MC_D2_PT_7_510
    );
  err_cs_or0003_MC_D2_PT_8 : X_AND5
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN3,
      I4 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN4,
      O => err_cs_or0003_MC_D2_PT_8_511
    );
  err_cs_or0003_MC_D2_PT_9 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN3,
      I4 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN4,
      I5 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN5,
      O => err_cs_or0003_MC_D2_PT_9_512
    );
  err_cs_or0003_MC_D2_PT_10 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN3,
      I4 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN4,
      I5 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN5,
      O => err_cs_or0003_MC_D2_PT_10_513
    );
  err_cs_or0003_MC_D2_PT_11 : X_AND6
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN2,
      I3 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN3,
      I4 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN4,
      I5 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN5,
      O => err_cs_or0003_MC_D2_PT_11_514
    );
  err_cs_or0003_MC_D2_PT_12 : X_AND6
    port map (
      I0 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN3,
      I4 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN4,
      I5 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN5,
      O => err_cs_or0003_MC_D2_PT_12_515
    );
  err_cs_or0003_MC_D2_PT_13 : X_AND7
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN0,
      I1 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN1,
      I2 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN3,
      I4 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN4,
      I5 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN5,
      I6 => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN6,
      O => err_cs_or0003_MC_D2_PT_13_516
    );
  err_cs_or0003_MC_D2 : X_OR16
    port map (
      I0 => NlwBufferSignal_err_cs_or0003_MC_D2_IN0,
      I1 => NlwBufferSignal_err_cs_or0003_MC_D2_IN1,
      I2 => NlwBufferSignal_err_cs_or0003_MC_D2_IN2,
      I3 => NlwBufferSignal_err_cs_or0003_MC_D2_IN3,
      I4 => NlwBufferSignal_err_cs_or0003_MC_D2_IN4,
      I5 => NlwBufferSignal_err_cs_or0003_MC_D2_IN5,
      I6 => NlwBufferSignal_err_cs_or0003_MC_D2_IN6,
      I7 => NlwBufferSignal_err_cs_or0003_MC_D2_IN7,
      I8 => NlwBufferSignal_err_cs_or0003_MC_D2_IN8,
      I9 => NlwBufferSignal_err_cs_or0003_MC_D2_IN9,
      I10 => NlwBufferSignal_err_cs_or0003_MC_D2_IN10,
      I11 => NlwBufferSignal_err_cs_or0003_MC_D2_IN11,
      I12 => NlwBufferSignal_err_cs_or0003_MC_D2_IN12,
      I13 => NlwBufferSignal_err_cs_or0003_MC_D2_IN13,
      I14 => NlwBufferSignal_err_cs_or0003_MC_D2_IN14,
      I15 => NlwBufferSignal_err_cs_or0003_MC_D2_IN15,
      O => err_cs_or0003_MC_D2_499
    );
  nClr_new_cs : X_BUF
    port map (
      I => nClr_new_cs_MC_Q,
      O => nClr_new_cs_500
    );
  nClr_new_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nClr_new_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_nClr_new_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => nClr_new_cs_MC_Q
    );
  nClr_new_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_nClr_new_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nClr_new_cs_MC_D_IN1,
      O => nClr_new_cs_MC_D_518
    );
  nClr_new_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nClr_new_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_nClr_new_cs_MC_D1_IN1,
      O => nClr_new_cs_MC_D1_519
    );
  nClr_new_cs_MC_D2 : X_ZERO
    port map (
      O => nClr_new_cs_MC_D2_520
    );
  nClr_old_cs : X_BUF
    port map (
      I => nClr_old_cs_MC_Q,
      O => nClr_old_cs_501
    );
  nClr_old_cs_MC_REG : X_FF
    generic map(
      INIT => '0'
    )
    port map (
      I => NlwBufferSignal_nClr_old_cs_MC_REG_IN,
      CE => Vcc_66,
      CLK => NlwBufferSignal_nClr_old_cs_MC_REG_CLK,
      SET => Gnd_65,
      RST => Gnd_65,
      O => nClr_old_cs_MC_Q
    );
  nClr_old_cs_MC_D : X_XOR2
    port map (
      I0 => NlwInverterSignal_nClr_old_cs_MC_D_IN0,
      I1 => NlwBufferSignal_nClr_old_cs_MC_D_IN1,
      O => nClr_old_cs_MC_D_522
    );
  nClr_old_cs_MC_D1 : X_AND2
    port map (
      I0 => NlwBufferSignal_nClr_old_cs_MC_D1_IN0,
      I1 => NlwInverterSignal_nClr_old_cs_MC_D1_IN1,
      O => nClr_old_cs_MC_D1_523
    );
  nClr_old_cs_MC_D2 : X_ZERO
    port map (
      O => nClr_old_cs_MC_D2_524
    );
  N_PZ_163 : X_BUF
    port map (
      I => N_PZ_163_MC_Q_525,
      O => N_PZ_163_479
    );
  N_PZ_163_MC_Q : X_BUF
    port map (
      I => N_PZ_163_MC_D_526,
      O => N_PZ_163_MC_Q_525
    );
  N_PZ_163_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_163_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_163_MC_D_IN1,
      O => N_PZ_163_MC_D_526
    );
  N_PZ_163_MC_D1 : X_ZERO
    port map (
      O => N_PZ_163_MC_D1_527
    );
  N_PZ_163_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN1,
      O => N_PZ_163_MC_D2_PT_0_529
    );
  N_PZ_163_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN1,
      O => N_PZ_163_MC_D2_PT_1_530
    );
  N_PZ_163_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_163_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_163_MC_D2_IN1,
      O => N_PZ_163_MC_D2_528
    );
  N_PZ_202 : X_BUF
    port map (
      I => N_PZ_202_MC_Q_531,
      O => N_PZ_202_480
    );
  N_PZ_202_MC_Q : X_BUF
    port map (
      I => N_PZ_202_MC_D_532,
      O => N_PZ_202_MC_Q_531
    );
  N_PZ_202_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_202_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_202_MC_D_IN1,
      O => N_PZ_202_MC_D_532
    );
  N_PZ_202_MC_D1 : X_ZERO
    port map (
      O => N_PZ_202_MC_D1_533
    );
  N_PZ_202_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_N_PZ_202_MC_D2_PT_0_IN1,
      O => N_PZ_202_MC_D2_PT_0_535
    );
  N_PZ_202_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN1,
      O => N_PZ_202_MC_D2_PT_1_536
    );
  N_PZ_202_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN1,
      I2 => NlwInverterSignal_N_PZ_202_MC_D2_PT_2_IN2,
      O => N_PZ_202_MC_D2_PT_2_537
    );
  N_PZ_202_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_N_PZ_202_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_202_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_202_MC_D2_IN2,
      O => N_PZ_202_MC_D2_534
    );
  N_PZ_250 : X_BUF
    port map (
      I => N_PZ_250_MC_Q_538,
      O => N_PZ_250_504
    );
  N_PZ_250_MC_Q : X_BUF
    port map (
      I => N_PZ_250_MC_D_539,
      O => N_PZ_250_MC_Q_538
    );
  N_PZ_250_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D_IN1,
      O => N_PZ_250_MC_D_539
    );
  N_PZ_250_MC_D1 : X_ZERO
    port map (
      O => N_PZ_250_MC_D1_540
    );
  N_PZ_250_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN1,
      O => N_PZ_250_MC_D2_PT_0_542
    );
  N_PZ_250_MC_D2_PT_1 : X_AND5
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN2,
      I3 => NlwInverterSignal_N_PZ_250_MC_D2_PT_1_IN3,
      I4 => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN4,
      O => N_PZ_250_MC_D2_PT_1_543
    );
  N_PZ_250_MC_D2_PT_2 : X_AND5
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN2,
      I3 => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN3,
      I4 => NlwInverterSignal_N_PZ_250_MC_D2_PT_2_IN4,
      O => N_PZ_250_MC_D2_PT_2_544
    );
  N_PZ_250_MC_D2_PT_3 : X_AND5
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN1,
      I2 => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN2,
      I3 => NlwInverterSignal_N_PZ_250_MC_D2_PT_3_IN3,
      I4 => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN4,
      O => N_PZ_250_MC_D2_PT_3_545
    );
  N_PZ_250_MC_D2_PT_4 : X_AND5
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN1,
      I2 => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN2,
      I3 => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN3,
      I4 => NlwInverterSignal_N_PZ_250_MC_D2_PT_4_IN4,
      O => N_PZ_250_MC_D2_PT_4_546
    );
  N_PZ_250_MC_D2 : X_OR5
    port map (
      I0 => NlwBufferSignal_N_PZ_250_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_250_MC_D2_IN1,
      I2 => NlwBufferSignal_N_PZ_250_MC_D2_IN2,
      I3 => NlwBufferSignal_N_PZ_250_MC_D2_IN3,
      I4 => NlwBufferSignal_N_PZ_250_MC_D2_IN4,
      O => N_PZ_250_MC_D2_541
    );
  N_PZ_264 : X_BUF
    port map (
      I => N_PZ_264_MC_Q_547,
      O => N_PZ_264_469
    );
  N_PZ_264_MC_Q : X_BUF
    port map (
      I => N_PZ_264_MC_D_548,
      O => N_PZ_264_MC_Q_547
    );
  N_PZ_264_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_N_PZ_264_MC_D_IN0,
      I1 => NlwBufferSignal_N_PZ_264_MC_D_IN1,
      O => N_PZ_264_MC_D_548
    );
  N_PZ_264_MC_D1 : X_ZERO
    port map (
      O => N_PZ_264_MC_D1_549
    );
  N_PZ_264_MC_D2_PT_0 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN0,
      I1 => NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN1,
      O => N_PZ_264_MC_D2_PT_0_551
    );
  N_PZ_264_MC_D2_PT_1 : X_AND2
    port map (
      I0 => NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_N_PZ_264_MC_D2_PT_1_IN1,
      O => N_PZ_264_MC_D2_PT_1_552
    );
  N_PZ_264_MC_D2 : X_OR2
    port map (
      I0 => NlwBufferSignal_N_PZ_264_MC_D2_IN0,
      I1 => NlwBufferSignal_N_PZ_264_MC_D2_IN1,
      O => N_PZ_264_MC_D2_550
    );
  err_cs_or000367 : X_BUF
    port map (
      I => err_cs_or000367_MC_Q_553,
      O => err_cs_or000367_474
    );
  err_cs_or000367_MC_Q : X_BUF
    port map (
      I => err_cs_or000367_MC_D_554,
      O => err_cs_or000367_MC_Q_553
    );
  err_cs_or000367_MC_D : X_XOR2
    port map (
      I0 => NlwBufferSignal_err_cs_or000367_MC_D_IN0,
      I1 => NlwBufferSignal_err_cs_or000367_MC_D_IN1,
      O => err_cs_or000367_MC_D_554
    );
  err_cs_or000367_MC_D1 : X_ZERO
    port map (
      O => err_cs_or000367_MC_D1_555
    );
  err_cs_or000367_MC_D2_PT_0 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN0,
      I1 => NlwInverterSignal_err_cs_or000367_MC_D2_PT_0_IN1,
      I2 => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN2,
      O => err_cs_or000367_MC_D2_PT_0_557
    );
  err_cs_or000367_MC_D2_PT_1 : X_AND3
    port map (
      I0 => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN0,
      I1 => NlwInverterSignal_err_cs_or000367_MC_D2_PT_1_IN1,
      I2 => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN2,
      O => err_cs_or000367_MC_D2_PT_1_558
    );
  err_cs_or000367_MC_D2_PT_2 : X_AND3
    port map (
      I0 => NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN0,
      I1 => NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN1,
      I2 => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN2,
      O => err_cs_or000367_MC_D2_PT_2_559
    );
  err_cs_or000367_MC_D2 : X_OR3
    port map (
      I0 => NlwBufferSignal_err_cs_or000367_MC_D2_IN0,
      I1 => NlwBufferSignal_err_cs_or000367_MC_D2_IN1,
      I2 => NlwBufferSignal_err_cs_or000367_MC_D2_IN2,
      O => err_cs_or000367_MC_D2_556
    );
  NlwBufferBlock_cnt_0_MC_REG_IN : X_BUF
    port map (
      I => cnt_0_MC_D_64,
      O => NlwBufferSignal_cnt_0_MC_REG_IN
    );
  NlwBufferBlock_cnt_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_0_MC_REG_CLK
    );
  NlwBufferBlock_cnt_0_MC_D_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D1_67,
      O => NlwBufferSignal_cnt_0_MC_D_IN0
    );
  NlwBufferBlock_cnt_0_MC_D_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_68,
      O => NlwBufferSignal_cnt_0_MC_D_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D1_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_0_MC_D1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D1_IN2 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_cnt_0_MC_D1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_0_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_0_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_0_73,
      O => NlwBufferSignal_cnt_0_MC_D2_IN0
    );
  NlwBufferBlock_cnt_0_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_0_MC_D2_PT_1_74,
      O => NlwBufferSignal_cnt_0_MC_D2_IN1
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D_IN0 : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_D1_77,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D_IN0
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D_IN1 : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_D2_78,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D_IN1
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => down_new_cs_79,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => up_new_cs_80,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => down_old_cs_81,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => down_new_cs_79,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => down_old_cs_81,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => up_old_cs_83,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_D2_PT_0_82,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN0
    );
  NlwBufferBlock_summand_v_mux0001_10_MC_D2_IN1 : X_BUF
    port map (
      I => summand_v_mux0001_10_MC_D2_PT_1_84,
      O => NlwBufferSignal_summand_v_mux0001_10_MC_D2_IN1
    );
  NlwBufferBlock_down_new_cs_MC_REG_IN : X_BUF
    port map (
      I => down_new_cs_MC_D_86,
      O => NlwBufferSignal_down_new_cs_MC_REG_IN
    );
  NlwBufferBlock_down_new_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_down_new_cs_MC_REG_CLK
    );
  NlwBufferBlock_down_new_cs_MC_D_IN0 : X_BUF
    port map (
      I => down_new_cs_MC_D1_87,
      O => NlwBufferSignal_down_new_cs_MC_D_IN0
    );
  NlwBufferBlock_down_new_cs_MC_D_IN1 : X_BUF
    port map (
      I => down_new_cs_MC_D2_88,
      O => NlwBufferSignal_down_new_cs_MC_D_IN1
    );
  NlwBufferBlock_down_new_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_down_new_cs_MC_D1_IN0
    );
  NlwBufferBlock_down_new_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_II_UIM_5,
      O => NlwBufferSignal_down_new_cs_MC_D1_IN1
    );
  NlwBufferBlock_up_new_cs_MC_REG_IN : X_BUF
    port map (
      I => up_new_cs_MC_D_90,
      O => NlwBufferSignal_up_new_cs_MC_REG_IN
    );
  NlwBufferBlock_up_new_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_up_new_cs_MC_REG_CLK
    );
  NlwBufferBlock_up_new_cs_MC_D_IN0 : X_BUF
    port map (
      I => up_new_cs_MC_D1_91,
      O => NlwBufferSignal_up_new_cs_MC_D_IN0
    );
  NlwBufferBlock_up_new_cs_MC_D_IN1 : X_BUF
    port map (
      I => up_new_cs_MC_D2_92,
      O => NlwBufferSignal_up_new_cs_MC_D_IN1
    );
  NlwBufferBlock_up_new_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_up_new_cs_MC_D1_IN0
    );
  NlwBufferBlock_up_new_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_II_UIM_7,
      O => NlwBufferSignal_up_new_cs_MC_D1_IN1
    );
  NlwBufferBlock_down_old_cs_MC_REG_IN : X_BUF
    port map (
      I => down_old_cs_MC_D_94,
      O => NlwBufferSignal_down_old_cs_MC_REG_IN
    );
  NlwBufferBlock_down_old_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_down_old_cs_MC_REG_CLK
    );
  NlwBufferBlock_down_old_cs_MC_D_IN0 : X_BUF
    port map (
      I => down_old_cs_MC_D1_95,
      O => NlwBufferSignal_down_old_cs_MC_D_IN0
    );
  NlwBufferBlock_down_old_cs_MC_D_IN1 : X_BUF
    port map (
      I => down_old_cs_MC_D2_96,
      O => NlwBufferSignal_down_old_cs_MC_D_IN1
    );
  NlwBufferBlock_down_old_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_down_old_cs_MC_D1_IN0
    );
  NlwBufferBlock_down_old_cs_MC_D1_IN1 : X_BUF
    port map (
      I => down_new_cs_79,
      O => NlwBufferSignal_down_old_cs_MC_D1_IN1
    );
  NlwBufferBlock_up_old_cs_MC_REG_IN : X_BUF
    port map (
      I => up_old_cs_MC_D_98,
      O => NlwBufferSignal_up_old_cs_MC_REG_IN
    );
  NlwBufferBlock_up_old_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_up_old_cs_MC_REG_CLK
    );
  NlwBufferBlock_up_old_cs_MC_D_IN0 : X_BUF
    port map (
      I => up_old_cs_MC_D1_99,
      O => NlwBufferSignal_up_old_cs_MC_D_IN0
    );
  NlwBufferBlock_up_old_cs_MC_D_IN1 : X_BUF
    port map (
      I => up_old_cs_MC_D2_100,
      O => NlwBufferSignal_up_old_cs_MC_D_IN1
    );
  NlwBufferBlock_up_old_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_up_old_cs_MC_D1_IN0
    );
  NlwBufferBlock_up_old_cs_MC_D1_IN1 : X_BUF
    port map (
      I => up_new_cs_80,
      O => NlwBufferSignal_up_old_cs_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_137_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_137_MC_D1_103,
      O => NlwBufferSignal_N_PZ_137_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_137_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_137_MC_D2_104,
      O => NlwBufferSignal_N_PZ_137_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => down_new_cs_79,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => up_new_cs_80,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => up_old_cs_83,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => up_new_cs_80,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => down_old_cs_81,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_137_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => up_old_cs_83,
      O => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_137_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_137_MC_D2_PT_0_105,
      O => NlwBufferSignal_N_PZ_137_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_137_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_137_MC_D2_PT_1_106,
      O => NlwBufferSignal_N_PZ_137_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_0_MC_D_108,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_0_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_0_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_0_MC_D1_109,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_0_MC_D2_110,
      O => NlwBufferSignal_valLd_cs_0_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_0_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_0_II_UIM_9,
      O => NlwBufferSignal_valLd_cs_0_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_133_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_133_MC_D1_113,
      O => NlwBufferSignal_N_PZ_133_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_133_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_133_MC_D2_114,
      O => NlwBufferSignal_N_PZ_133_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_133_MC_D1_IN0 : X_BUF
    port map (
      I => nLd_new_cs_115,
      O => NlwBufferSignal_N_PZ_133_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_133_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_old_cs_116,
      O => NlwBufferSignal_N_PZ_133_MC_D1_IN1
    );
  NlwBufferBlock_nLd_new_cs_MC_REG_IN : X_BUF
    port map (
      I => nLd_new_cs_MC_D_118,
      O => NlwBufferSignal_nLd_new_cs_MC_REG_IN
    );
  NlwBufferBlock_nLd_new_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nLd_new_cs_MC_REG_CLK
    );
  NlwBufferBlock_nLd_new_cs_MC_D_IN0 : X_BUF
    port map (
      I => nLd_new_cs_MC_D1_119,
      O => NlwBufferSignal_nLd_new_cs_MC_D_IN0
    );
  NlwBufferBlock_nLd_new_cs_MC_D_IN1 : X_BUF
    port map (
      I => nLd_new_cs_MC_D2_120,
      O => NlwBufferSignal_nLd_new_cs_MC_D_IN1
    );
  NlwBufferBlock_nLd_new_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nLd_new_cs_MC_D1_IN0
    );
  NlwBufferBlock_nLd_new_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_II_UIM_11,
      O => NlwBufferSignal_nLd_new_cs_MC_D1_IN1
    );
  NlwBufferBlock_nLd_old_cs_MC_REG_IN : X_BUF
    port map (
      I => nLd_old_cs_MC_D_122,
      O => NlwBufferSignal_nLd_old_cs_MC_REG_IN
    );
  NlwBufferBlock_nLd_old_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nLd_old_cs_MC_REG_CLK
    );
  NlwBufferBlock_nLd_old_cs_MC_D_IN0 : X_BUF
    port map (
      I => nLd_old_cs_MC_D1_123,
      O => NlwBufferSignal_nLd_old_cs_MC_D_IN0
    );
  NlwBufferBlock_nLd_old_cs_MC_D_IN1 : X_BUF
    port map (
      I => nLd_old_cs_MC_D2_124,
      O => NlwBufferSignal_nLd_old_cs_MC_D_IN1
    );
  NlwBufferBlock_nLd_old_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nLd_old_cs_MC_D1_IN0
    );
  NlwBufferBlock_nLd_old_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nLd_new_cs_115,
      O => NlwBufferSignal_nLd_old_cs_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_MC_D1_127,
      O => NlwBufferSignal_cnt_10_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_MC_D2_128,
      O => NlwBufferSignal_cnt_10_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_cnt_10_MC_D1_IN0
    );
  NlwBufferBlock_cnt_10_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_cnt_10_MC_D1_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D_131,
      O => NlwBufferSignal_cnt_10_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_10_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_10_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D1_132,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_133,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_0_135,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_1_138,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_10_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_MC_D2_PT_2_139,
      O => NlwBufferSignal_cnt_10_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D1_142,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_143,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN6 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_146,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_152,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_154,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_ns_9_MC_D1_157,
      O => NlwBufferSignal_cnt_ns_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_158,
      O => NlwBufferSignal_cnt_ns_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D1_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D1_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_PT_0_159,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_PT_1_161,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_IN1
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_PT_2_162,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_IN2
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_PT_3_163,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_IN3
    );
  NlwBufferBlock_cnt_ns_9_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_ns_9_MC_D2_PT_4_164,
      O => NlwBufferSignal_cnt_ns_9_MC_D2_IN4
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_D1_167,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_D2_168,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(9),
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_171,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_D2_PT_0_170,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_9_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_9_MC_D2_PT_1_172,
      O => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_9_MC_D_174,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_9_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_9_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_9_MC_D1_175,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_9_MC_D2_176,
      O => NlwBufferSignal_valLd_cs_9_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_9_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_9_II_UIM_13,
      O => NlwBufferSignal_valLd_cs_9_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D_178,
      O => NlwBufferSignal_cnt_9_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_9_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_9_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D1_179,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_MC_D2_180,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_9_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_cnt_9_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_D1_183,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_D2_184,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(5),
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_187,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_D2_PT_0_186,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_5_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_MC_D2_PT_1_188,
      O => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_5_MC_D_190,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_5_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_5_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_5_MC_D1_191,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_5_MC_D2_192,
      O => NlwBufferSignal_valLd_cs_5_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_5_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_5_II_UIM_15,
      O => NlwBufferSignal_valLd_cs_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D_194,
      O => NlwBufferSignal_cnt_5_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_5_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_5_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D1_195,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_196,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_0_198,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_5_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_MC_D2_PT_1_199,
      O => NlwBufferSignal_cnt_5_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_138_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_138_MC_D1_202,
      O => NlwBufferSignal_N_PZ_138_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_138_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_138_MC_D2_203,
      O => NlwBufferSignal_N_PZ_138_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_138_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_138_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_138_MC_D2_PT_0_206,
      O => NlwBufferSignal_N_PZ_138_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_138_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_138_MC_D2_PT_1_208,
      O => NlwBufferSignal_N_PZ_138_MC_D2_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D_IN0 : X_BUF
    port map (
      I => err_cs_and000072_MC_D1_211,
      O => NlwBufferSignal_err_cs_and000072_MC_D_IN0
    );
  NlwBufferBlock_err_cs_and000072_MC_D_IN1 : X_BUF
    port map (
      I => err_cs_and000072_MC_D2_212,
      O => NlwBufferSignal_err_cs_and000072_MC_D_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_err_cs_and000072_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_IN0 : X_BUF
    port map (
      I => err_cs_and000072_MC_D2_PT_0_213,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_IN0
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_IN1 : X_BUF
    port map (
      I => err_cs_and000072_MC_D2_PT_1_216,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_IN1
    );
  NlwBufferBlock_err_cs_and000072_MC_D2_IN2 : X_BUF
    port map (
      I => err_cs_and000072_MC_D2_PT_2_220,
      O => NlwBufferSignal_err_cs_and000072_MC_D2_IN2
    );
  NlwBufferBlock_SF4_MC_D_IN0 : X_BUF
    port map (
      I => SF4_MC_D1_223,
      O => NlwBufferSignal_SF4_MC_D_IN0
    );
  NlwBufferBlock_SF4_MC_D_IN1 : X_BUF
    port map (
      I => SF4_MC_D2_224,
      O => NlwBufferSignal_SF4_MC_D_IN1
    );
  NlwBufferBlock_SF4_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_SF4_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_SF4_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_SF4_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_SF4_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_SF4_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_SF4_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_SF4_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_SF4_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_SF4_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_SF4_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_SF4_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_SF4_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_SF4_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_SF4_MC_D2_IN0 : X_BUF
    port map (
      I => SF4_MC_D2_PT_0_226,
      O => NlwBufferSignal_SF4_MC_D2_IN0
    );
  NlwBufferBlock_SF4_MC_D2_IN1 : X_BUF
    port map (
      I => SF4_MC_D2_PT_1_227,
      O => NlwBufferSignal_SF4_MC_D2_IN1
    );
  NlwBufferBlock_SF4_MC_D2_IN2 : X_BUF
    port map (
      I => SF4_MC_D2_PT_2_229,
      O => NlwBufferSignal_SF4_MC_D2_IN2
    );
  NlwBufferBlock_SF4_MC_D2_IN3 : X_BUF
    port map (
      I => SF4_MC_D2_PT_3_230,
      O => NlwBufferSignal_SF4_MC_D2_IN3
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_1_MC_D_232,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_1_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_1_MC_D1_233,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_1_MC_D2_234,
      O => NlwBufferSignal_valLd_cs_1_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_1_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_1_II_UIM_17,
      O => NlwBufferSignal_valLd_cs_1_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_2_MC_D_236,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_2_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_2_MC_D1_237,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_2_MC_D2_238,
      O => NlwBufferSignal_valLd_cs_2_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_2_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_2_II_UIM_19,
      O => NlwBufferSignal_valLd_cs_2_MC_D1_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_3_MC_D_240,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_3_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_3_MC_D1_241,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_3_MC_D2_242,
      O => NlwBufferSignal_valLd_cs_3_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_3_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_3_II_UIM_21,
      O => NlwBufferSignal_valLd_cs_3_MC_D1_IN1
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D_244,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN0
    );
  NlwBufferBlock_cnt_1_MC_tsimcreated_xor_IN1 : X_BUF
    port map (
      I => cnt_1_MC_Q_tsimrenamed_net_Q,
      O => NlwBufferSignal_cnt_1_MC_tsimcreated_xor_IN1
    );
  NlwBufferBlock_cnt_1_MC_REG_IN : X_BUF
    port map (
      I => cnt_1_MC_tsimcreated_xor_Q_245,
      O => NlwBufferSignal_cnt_1_MC_REG_IN
    );
  NlwBufferBlock_cnt_1_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_1_MC_REG_CLK
    );
  NlwBufferBlock_cnt_1_MC_D_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D1_246,
      O => NlwBufferSignal_cnt_1_MC_D_IN0
    );
  NlwBufferBlock_cnt_1_MC_D_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_247,
      O => NlwBufferSignal_cnt_1_MC_D_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_232_249,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_232_249,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_232_249,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => N_PZ_232_249,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_PT_7_IN5 : X_BUF
    port map (
      I => N_PZ_232_249,
      O => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_0_248,
      O => NlwBufferSignal_cnt_1_MC_D2_IN0
    );
  NlwBufferBlock_cnt_1_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_1_250,
      O => NlwBufferSignal_cnt_1_MC_D2_IN1
    );
  NlwBufferBlock_cnt_1_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_2_251,
      O => NlwBufferSignal_cnt_1_MC_D2_IN2
    );
  NlwBufferBlock_cnt_1_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_3_252,
      O => NlwBufferSignal_cnt_1_MC_D2_IN3
    );
  NlwBufferBlock_cnt_1_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_4_253,
      O => NlwBufferSignal_cnt_1_MC_D2_IN4
    );
  NlwBufferBlock_cnt_1_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_5_254,
      O => NlwBufferSignal_cnt_1_MC_D2_IN5
    );
  NlwBufferBlock_cnt_1_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_6_255,
      O => NlwBufferSignal_cnt_1_MC_D2_IN6
    );
  NlwBufferBlock_cnt_1_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_1_MC_D2_PT_7_256,
      O => NlwBufferSignal_cnt_1_MC_D2_IN7
    );
  NlwBufferBlock_N_PZ_232_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_232_MC_D1_259,
      O => NlwBufferSignal_N_PZ_232_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_232_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_232_MC_D2_260,
      O => NlwBufferSignal_N_PZ_232_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_232_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_232_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_232_MC_D2_PT_0_261,
      O => NlwBufferSignal_N_PZ_232_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_232_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_232_MC_D2_PT_1_262,
      O => NlwBufferSignal_N_PZ_232_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_REG_IN : X_BUF
    port map (
      I => cnt_2_MC_D_264,
      O => NlwBufferSignal_cnt_2_MC_REG_IN
    );
  NlwBufferBlock_cnt_2_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_2_MC_REG_CLK
    );
  NlwBufferBlock_cnt_2_MC_D_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D1_265,
      O => NlwBufferSignal_cnt_2_MC_D_IN0
    );
  NlwBufferBlock_cnt_2_MC_D_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_266,
      O => NlwBufferSignal_cnt_2_MC_D_IN1
    );
  NlwBufferBlock_cnt_2_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D1_IN2 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_2_MC_D1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_PT_6_IN5 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_0_267,
      O => NlwBufferSignal_cnt_2_MC_D2_IN0
    );
  NlwBufferBlock_cnt_2_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_1_268,
      O => NlwBufferSignal_cnt_2_MC_D2_IN1
    );
  NlwBufferBlock_cnt_2_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_2_269,
      O => NlwBufferSignal_cnt_2_MC_D2_IN2
    );
  NlwBufferBlock_cnt_2_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_3_270,
      O => NlwBufferSignal_cnt_2_MC_D2_IN3
    );
  NlwBufferBlock_cnt_2_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_4_271,
      O => NlwBufferSignal_cnt_2_MC_D2_IN4
    );
  NlwBufferBlock_cnt_2_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_5_272,
      O => NlwBufferSignal_cnt_2_MC_D2_IN5
    );
  NlwBufferBlock_cnt_2_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_2_MC_D2_PT_6_273,
      O => NlwBufferSignal_cnt_2_MC_D2_IN6
    );
  NlwBufferBlock_N_PZ_294_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_294_MC_D1_276,
      O => NlwBufferSignal_N_PZ_294_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_294_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_294_MC_D2_277,
      O => NlwBufferSignal_N_PZ_294_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_294_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_294_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_294_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_N_PZ_294_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_294_MC_D1_IN2 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_N_PZ_294_MC_D1_IN2
    );
  NlwBufferBlock_cnt_3_MC_REG_IN : X_BUF
    port map (
      I => cnt_3_MC_D_279,
      O => NlwBufferSignal_cnt_3_MC_REG_IN
    );
  NlwBufferBlock_cnt_3_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_3_MC_REG_CLK
    );
  NlwBufferBlock_cnt_3_MC_D_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D1_280,
      O => NlwBufferSignal_cnt_3_MC_D_IN0
    );
  NlwBufferBlock_cnt_3_MC_D_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_281,
      O => NlwBufferSignal_cnt_3_MC_D_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => valLd_cs(0),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => valLd_cs(1),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_6_IN4 : X_BUF
    port map (
      I => valLd_cs(3),
      O => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => cnt_0_MC_UIM_63,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => N_PZ_294_215,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_7_IN4 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => cnt_3_MC_UIM_225,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => cnt_1_MC_UIM_228,
      O => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_0_282,
      O => NlwBufferSignal_cnt_3_MC_D2_IN0
    );
  NlwBufferBlock_cnt_3_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_1_283,
      O => NlwBufferSignal_cnt_3_MC_D2_IN1
    );
  NlwBufferBlock_cnt_3_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_2_284,
      O => NlwBufferSignal_cnt_3_MC_D2_IN2
    );
  NlwBufferBlock_cnt_3_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_3_285,
      O => NlwBufferSignal_cnt_3_MC_D2_IN3
    );
  NlwBufferBlock_cnt_3_MC_D2_IN4 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_4_286,
      O => NlwBufferSignal_cnt_3_MC_D2_IN4
    );
  NlwBufferBlock_cnt_3_MC_D2_IN5 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_5_287,
      O => NlwBufferSignal_cnt_3_MC_D2_IN5
    );
  NlwBufferBlock_cnt_3_MC_D2_IN6 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_6_288,
      O => NlwBufferSignal_cnt_3_MC_D2_IN6
    );
  NlwBufferBlock_cnt_3_MC_D2_IN7 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_7_289,
      O => NlwBufferSignal_cnt_3_MC_D2_IN7
    );
  NlwBufferBlock_cnt_3_MC_D2_IN8 : X_BUF
    port map (
      I => cnt_3_MC_D2_PT_8_290,
      O => NlwBufferSignal_cnt_3_MC_D2_IN8
    );
  NlwBufferBlock_cnt_3_MC_D2_IN9 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN9
    );
  NlwBufferBlock_cnt_3_MC_D2_IN10 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN10
    );
  NlwBufferBlock_cnt_3_MC_D2_IN11 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN11
    );
  NlwBufferBlock_cnt_3_MC_D2_IN12 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN12
    );
  NlwBufferBlock_cnt_3_MC_D2_IN13 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN13
    );
  NlwBufferBlock_cnt_3_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN14
    );
  NlwBufferBlock_cnt_3_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_cnt_3_MC_D2_IN15
    );
  NlwBufferBlock_N_PZ_135_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_135_MC_D1_293,
      O => NlwBufferSignal_N_PZ_135_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_135_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_135_MC_D2_294,
      O => NlwBufferSignal_N_PZ_135_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_135_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_135_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(4),
      O => NlwBufferSignal_N_PZ_135_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_135_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_135_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_297,
      O => NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_135_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_135_MC_D2_PT_0_296,
      O => NlwBufferSignal_N_PZ_135_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_135_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_135_MC_D2_PT_1_298,
      O => NlwBufferSignal_N_PZ_135_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_4_MC_D_300,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_4_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_4_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_4_MC_D1_301,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_4_MC_D2_302,
      O => NlwBufferSignal_valLd_cs_4_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_4_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_4_II_UIM_23,
      O => NlwBufferSignal_valLd_cs_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D_304,
      O => NlwBufferSignal_cnt_4_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_4_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_4_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D1_305,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_306,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_0_307,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_1_308,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_2_309,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_4_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_4_BUFR_MC_D2_PT_3_310,
      O => NlwBufferSignal_cnt_4_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_SF87_MC_D_IN0 : X_BUF
    port map (
      I => SF87_MC_D1_313,
      O => NlwBufferSignal_SF87_MC_D_IN0
    );
  NlwBufferBlock_SF87_MC_D_IN1 : X_BUF
    port map (
      I => SF87_MC_D2_314,
      O => NlwBufferSignal_SF87_MC_D_IN1
    );
  NlwBufferBlock_SF87_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_SF87_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_SF87_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_SF87_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_SF87_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_SF87_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_SF87_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_SF87_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_SF87_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_SF87_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_SF87_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_SF87_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_SF87_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_SF87_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_SF87_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_SF87_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_SF87_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_SF87_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_SF87_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_SF87_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_SF87_MC_D2_IN0 : X_BUF
    port map (
      I => SF87_MC_D2_PT_0_315,
      O => NlwBufferSignal_SF87_MC_D2_IN0
    );
  NlwBufferBlock_SF87_MC_D2_IN1 : X_BUF
    port map (
      I => SF87_MC_D2_PT_1_316,
      O => NlwBufferSignal_SF87_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_139_MC_D1_319,
      O => NlwBufferSignal_N_PZ_139_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_320,
      O => NlwBufferSignal_N_PZ_139_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_6_BUFR_323,
      O => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_139_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_PT_0_322,
      O => NlwBufferSignal_N_PZ_139_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_139_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_139_MC_D2_PT_1_324,
      O => NlwBufferSignal_N_PZ_139_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_6_MC_D_326,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_6_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_6_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_6_MC_D1_327,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_6_MC_D2_328,
      O => NlwBufferSignal_valLd_cs_6_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_6_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_6_II_UIM_25,
      O => NlwBufferSignal_valLd_cs_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_6_BUFR_MC_D_330,
      O => NlwBufferSignal_cnt_6_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_6_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_6_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_BUFR_MC_D1_331,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_BUFR_MC_D2_332,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_6_BUFR_MC_D2_PT_0_333,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_6_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_6_BUFR_MC_D2_PT_1_334,
      O => NlwBufferSignal_cnt_6_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_253_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_253_MC_D1_337,
      O => NlwBufferSignal_N_PZ_253_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_253_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_253_MC_D2_338,
      O => NlwBufferSignal_N_PZ_253_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_253_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_253_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_253_MC_D2_PT_0_339,
      O => NlwBufferSignal_N_PZ_253_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_253_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_253_MC_D2_PT_1_340,
      O => NlwBufferSignal_N_PZ_253_MC_D2_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_D1_343,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_D2_344,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(8),
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_347,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_D2_PT_0_346,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN0
    );
  NlwBufferBlock_cnt_v_mux0000_8_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_8_MC_D2_PT_1_348,
      O => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_8_MC_D_350,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_8_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_8_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_8_MC_D1_351,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_8_MC_D2_352,
      O => NlwBufferSignal_valLd_cs_8_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_8_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_8_II_UIM_27,
      O => NlwBufferSignal_valLd_cs_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D_354,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_8_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_8_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D1_355,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_356,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_0_358,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_1_359,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_2_360,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_cnt_8_BUFR_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_8_BUFR_MC_D2_PT_3_361,
      O => NlwBufferSignal_cnt_8_BUFR_MC_D2_IN3
    );
  NlwBufferBlock_cnt_ns_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_ns_7_MC_D1_364,
      O => NlwBufferSignal_cnt_ns_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_ns_7_MC_D2_365,
      O => NlwBufferSignal_cnt_ns_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D1_IN0 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_ns_7_MC_D2_PT_0_366,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_IN0
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_ns_7_MC_D2_PT_1_367,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_IN1
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_ns_7_MC_D2_PT_2_368,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_IN2
    );
  NlwBufferBlock_cnt_ns_7_MC_D2_IN3 : X_BUF
    port map (
      I => cnt_ns_7_MC_D2_PT_3_369,
      O => NlwBufferSignal_cnt_ns_7_MC_D2_IN3
    );
  NlwBufferBlock_N_PZ_134_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_134_MC_D1_372,
      O => NlwBufferSignal_N_PZ_134_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_134_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_134_MC_D2_373,
      O => NlwBufferSignal_N_PZ_134_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_134_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_134_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_N_PZ_134_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_134_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_134_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_376,
      O => NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_134_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_134_MC_D2_PT_0_375,
      O => NlwBufferSignal_N_PZ_134_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_134_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_134_MC_D2_PT_1_377,
      O => NlwBufferSignal_N_PZ_134_MC_D2_IN1
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_7_MC_D_379,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_7_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_7_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_7_MC_D1_380,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_7_MC_D2_381,
      O => NlwBufferSignal_valLd_cs_7_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_7_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_7_II_UIM_29,
      O => NlwBufferSignal_valLd_cs_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D_383,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_7_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_7_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D1_384,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_MC_D2_385,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_BUFR_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_cnt_7_BUFR_MC_D1_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D1_388,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_389,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN9 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN9
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN10 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN10
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN11 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN11
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN12 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN12
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN13 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN13
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN14 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN14
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN15 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN15
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => cnt_6_BUFR_323,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => cnt_7_BUFR_376,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => valLd_cs(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN6 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN7 : X_BUF
    port map (
      I => valLd_cs(6),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN7
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN8 : X_BUF
    port map (
      I => valLd_cs(7),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN8
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN9 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN9
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN10 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN10
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN11 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN11
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN12 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN12
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN13 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN13
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN14 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN14
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN15 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN15
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => cnt_10_BUFR_129,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN6 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN6
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN7 : X_BUF
    port map (
      I => cnt_6_BUFR_323,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN7
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN8 : X_BUF
    port map (
      I => cnt_7_BUFR_376,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN8
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN9 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN9
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN10 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN10
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN11 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN11
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN12 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN12
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN13 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN13
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN14 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN14
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN15 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN15
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_390,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN0
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_391,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN1
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_392,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN2
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN3 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_393,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN3
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN4 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_394,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN4
    );
  NlwBufferBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN5 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_395,
      O => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_IN5
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_10_MC_D_397,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_10_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_10_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_10_MC_D1_398,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_10_MC_D2_399,
      O => NlwBufferSignal_valLd_cs_10_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_10_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_10_II_UIM_31,
      O => NlwBufferSignal_valLd_cs_10_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_190_MC_D1_402,
      O => NlwBufferSignal_N_PZ_190_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_190_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_190_MC_D2_403,
      O => NlwBufferSignal_N_PZ_190_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_N_PZ_190_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_N_PZ_190_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_190_MC_D2_PT_0_404,
      O => NlwBufferSignal_N_PZ_190_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_190_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_190_MC_D2_PT_1_405,
      O => NlwBufferSignal_N_PZ_190_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_190_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_190_MC_D2_PT_2_406,
      O => NlwBufferSignal_N_PZ_190_MC_D2_IN2
    );
  NlwBufferBlock_cnt_11_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_MC_D1_409,
      O => NlwBufferSignal_cnt_11_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_MC_D2_410,
      O => NlwBufferSignal_cnt_11_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_11_BUFR_411,
      O => NlwBufferSignal_cnt_11_MC_D1_IN0
    );
  NlwBufferBlock_cnt_11_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_11_BUFR_411,
      O => NlwBufferSignal_cnt_11_MC_D1_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_REG_IN : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D_413,
      O => NlwBufferSignal_cnt_11_BUFR_MC_REG_IN
    );
  NlwBufferBlock_cnt_11_BUFR_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_cnt_11_BUFR_MC_REG_CLK
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D_IN0 : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D1_414,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D_IN0
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D_IN1 : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D2_415,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_11_BUFR_411,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_IN0 : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D2_PT_0_417,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN0
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_IN1 : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D2_PT_1_420,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN1
    );
  NlwBufferBlock_cnt_11_BUFR_MC_D2_IN2 : X_BUF
    port map (
      I => cnt_11_BUFR_MC_D2_PT_2_421,
      O => NlwBufferSignal_cnt_11_BUFR_MC_D2_IN2
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_IN : X_BUF
    port map (
      I => valLd_cs_11_MC_D_423,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_IN
    );
  NlwBufferBlock_valLd_cs_11_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_valLd_cs_11_MC_REG_CLK
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN0 : X_BUF
    port map (
      I => valLd_cs_11_MC_D1_424,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D_IN1 : X_BUF
    port map (
      I => valLd_cs_11_MC_D2_425,
      O => NlwBufferSignal_valLd_cs_11_MC_D_IN1
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN0
    );
  NlwBufferBlock_valLd_cs_11_MC_D1_IN1 : X_BUF
    port map (
      I => valLd_11_II_UIM_33,
      O => NlwBufferSignal_valLd_cs_11_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_155_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_155_MC_D1_428,
      O => NlwBufferSignal_N_PZ_155_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_155_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_155_MC_D2_429,
      O => NlwBufferSignal_N_PZ_155_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_155_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_155_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_155_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_155_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_155_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_155_MC_D2_PT_0_430,
      O => NlwBufferSignal_N_PZ_155_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_155_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_155_MC_D2_PT_1_431,
      O => NlwBufferSignal_N_PZ_155_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_266_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_266_MC_D1_434,
      O => NlwBufferSignal_N_PZ_266_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_266_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_266_MC_D2_435,
      O => NlwBufferSignal_N_PZ_266_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => valLd_cs(11),
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_11_BUFR_411,
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_266_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_266_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_266_MC_D2_PT_0_436,
      O => NlwBufferSignal_N_PZ_266_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_266_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_266_MC_D2_PT_1_437,
      O => NlwBufferSignal_N_PZ_266_MC_D2_IN1
    );
  NlwBufferBlock_cnt_4_MC_D_IN0 : X_BUF
    port map (
      I => cnt_4_MC_D1_440,
      O => NlwBufferSignal_cnt_4_MC_D_IN0
    );
  NlwBufferBlock_cnt_4_MC_D_IN1 : X_BUF
    port map (
      I => cnt_4_MC_D2_441,
      O => NlwBufferSignal_cnt_4_MC_D_IN1
    );
  NlwBufferBlock_cnt_4_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_4_BUFR_297,
      O => NlwBufferSignal_cnt_4_MC_D1_IN0
    );
  NlwBufferBlock_cnt_4_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_4_BUFR_297,
      O => NlwBufferSignal_cnt_4_MC_D1_IN1
    );
  NlwBufferBlock_cnt_5_MC_D_IN0 : X_BUF
    port map (
      I => cnt_5_MC_D1_444,
      O => NlwBufferSignal_cnt_5_MC_D_IN0
    );
  NlwBufferBlock_cnt_5_MC_D_IN1 : X_BUF
    port map (
      I => cnt_5_MC_D2_445,
      O => NlwBufferSignal_cnt_5_MC_D_IN1
    );
  NlwBufferBlock_cnt_5_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_5_BUFR_187,
      O => NlwBufferSignal_cnt_5_MC_D1_IN0
    );
  NlwBufferBlock_cnt_5_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_5_BUFR_187,
      O => NlwBufferSignal_cnt_5_MC_D1_IN1
    );
  NlwBufferBlock_cnt_6_MC_D_IN0 : X_BUF
    port map (
      I => cnt_6_MC_D1_448,
      O => NlwBufferSignal_cnt_6_MC_D_IN0
    );
  NlwBufferBlock_cnt_6_MC_D_IN1 : X_BUF
    port map (
      I => cnt_6_MC_D2_449,
      O => NlwBufferSignal_cnt_6_MC_D_IN1
    );
  NlwBufferBlock_cnt_6_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_6_BUFR_323,
      O => NlwBufferSignal_cnt_6_MC_D1_IN0
    );
  NlwBufferBlock_cnt_6_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_6_BUFR_323,
      O => NlwBufferSignal_cnt_6_MC_D1_IN1
    );
  NlwBufferBlock_cnt_7_MC_D_IN0 : X_BUF
    port map (
      I => cnt_7_MC_D1_452,
      O => NlwBufferSignal_cnt_7_MC_D_IN0
    );
  NlwBufferBlock_cnt_7_MC_D_IN1 : X_BUF
    port map (
      I => cnt_7_MC_D2_453,
      O => NlwBufferSignal_cnt_7_MC_D_IN1
    );
  NlwBufferBlock_cnt_7_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_7_BUFR_376,
      O => NlwBufferSignal_cnt_7_MC_D1_IN0
    );
  NlwBufferBlock_cnt_7_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_7_BUFR_376,
      O => NlwBufferSignal_cnt_7_MC_D1_IN1
    );
  NlwBufferBlock_cnt_8_MC_D_IN0 : X_BUF
    port map (
      I => cnt_8_MC_D1_456,
      O => NlwBufferSignal_cnt_8_MC_D_IN0
    );
  NlwBufferBlock_cnt_8_MC_D_IN1 : X_BUF
    port map (
      I => cnt_8_MC_D2_457,
      O => NlwBufferSignal_cnt_8_MC_D_IN1
    );
  NlwBufferBlock_cnt_8_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_8_BUFR_347,
      O => NlwBufferSignal_cnt_8_MC_D1_IN0
    );
  NlwBufferBlock_cnt_8_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_8_BUFR_347,
      O => NlwBufferSignal_cnt_8_MC_D1_IN1
    );
  NlwBufferBlock_cnt_9_MC_D_IN0 : X_BUF
    port map (
      I => cnt_9_MC_D1_460,
      O => NlwBufferSignal_cnt_9_MC_D_IN0
    );
  NlwBufferBlock_cnt_9_MC_D_IN1 : X_BUF
    port map (
      I => cnt_9_MC_D2_461,
      O => NlwBufferSignal_cnt_9_MC_D_IN1
    );
  NlwBufferBlock_cnt_9_MC_D1_IN0 : X_BUF
    port map (
      I => cnt_9_BUFR_171,
      O => NlwBufferSignal_cnt_9_MC_D1_IN0
    );
  NlwBufferBlock_cnt_9_MC_D1_IN1 : X_BUF
    port map (
      I => cnt_9_BUFR_171,
      O => NlwBufferSignal_cnt_9_MC_D1_IN1
    );
  NlwBufferBlock_err_MC_REG_IN : X_BUF
    port map (
      I => err_MC_D_464,
      O => NlwBufferSignal_err_MC_REG_IN
    );
  NlwBufferBlock_err_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_err_MC_REG_CLK
    );
  NlwBufferBlock_err_MC_D_IN0 : X_BUF
    port map (
      I => err_MC_D1_465,
      O => NlwBufferSignal_err_MC_D_IN0
    );
  NlwBufferBlock_err_MC_D_IN1 : X_BUF
    port map (
      I => err_MC_D2_466,
      O => NlwBufferSignal_err_MC_D_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => err_MC_UIM_463,
      O => NlwBufferSignal_err_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_err_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_err_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_247_467,
      O => NlwBufferSignal_err_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_err_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_err_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_10_IN5 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_10_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN2 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN3 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_err_MC_D2_PT_11_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN4 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN5 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_11_IN6 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_11_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN1 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN2 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN3 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN4 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN5 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_12_IN6 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_12_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_MC_D2_PT_13_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN3 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN4 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN5 : X_BUF
    port map (
      I => valLd_cs(2),
      O => NlwBufferSignal_err_MC_D2_PT_13_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN6 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_13_IN7 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_13_IN7
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN0
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN1 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_MC_D2_PT_14_IN1
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN2 : X_BUF
    port map (
      I => N_PZ_133_72,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN2
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN3 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN3
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN4 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN4
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN5 : X_BUF
    port map (
      I => cnt_2_MC_UIM_214,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN5
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN6 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN6
    );
  NlwBufferBlock_err_MC_D2_PT_14_IN7 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_MC_D2_PT_14_IN7
    );
  NlwBufferBlock_err_MC_D2_IN0 : X_BUF
    port map (
      I => err_MC_D2_PT_0_468,
      O => NlwBufferSignal_err_MC_D2_IN0
    );
  NlwBufferBlock_err_MC_D2_IN1 : X_BUF
    port map (
      I => err_MC_D2_PT_1_470,
      O => NlwBufferSignal_err_MC_D2_IN1
    );
  NlwBufferBlock_err_MC_D2_IN2 : X_BUF
    port map (
      I => err_MC_D2_PT_2_472,
      O => NlwBufferSignal_err_MC_D2_IN2
    );
  NlwBufferBlock_err_MC_D2_IN3 : X_BUF
    port map (
      I => err_MC_D2_PT_3_473,
      O => NlwBufferSignal_err_MC_D2_IN3
    );
  NlwBufferBlock_err_MC_D2_IN4 : X_BUF
    port map (
      I => err_MC_D2_PT_4_475,
      O => NlwBufferSignal_err_MC_D2_IN4
    );
  NlwBufferBlock_err_MC_D2_IN5 : X_BUF
    port map (
      I => err_MC_D2_PT_5_476,
      O => NlwBufferSignal_err_MC_D2_IN5
    );
  NlwBufferBlock_err_MC_D2_IN6 : X_BUF
    port map (
      I => err_MC_D2_PT_6_477,
      O => NlwBufferSignal_err_MC_D2_IN6
    );
  NlwBufferBlock_err_MC_D2_IN7 : X_BUF
    port map (
      I => err_MC_D2_PT_7_478,
      O => NlwBufferSignal_err_MC_D2_IN7
    );
  NlwBufferBlock_err_MC_D2_IN8 : X_BUF
    port map (
      I => err_MC_D2_PT_8_481,
      O => NlwBufferSignal_err_MC_D2_IN8
    );
  NlwBufferBlock_err_MC_D2_IN9 : X_BUF
    port map (
      I => err_MC_D2_PT_9_482,
      O => NlwBufferSignal_err_MC_D2_IN9
    );
  NlwBufferBlock_err_MC_D2_IN10 : X_BUF
    port map (
      I => err_MC_D2_PT_10_483,
      O => NlwBufferSignal_err_MC_D2_IN10
    );
  NlwBufferBlock_err_MC_D2_IN11 : X_BUF
    port map (
      I => err_MC_D2_PT_11_484,
      O => NlwBufferSignal_err_MC_D2_IN11
    );
  NlwBufferBlock_err_MC_D2_IN12 : X_BUF
    port map (
      I => err_MC_D2_PT_12_485,
      O => NlwBufferSignal_err_MC_D2_IN12
    );
  NlwBufferBlock_err_MC_D2_IN13 : X_BUF
    port map (
      I => err_MC_D2_PT_13_486,
      O => NlwBufferSignal_err_MC_D2_IN13
    );
  NlwBufferBlock_err_MC_D2_IN14 : X_BUF
    port map (
      I => err_MC_D2_PT_14_487,
      O => NlwBufferSignal_err_MC_D2_IN14
    );
  NlwBufferBlock_err_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_err_MC_D2_IN15
    );
  NlwBufferBlock_N_PZ_247_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_247_MC_D1_490,
      O => NlwBufferSignal_N_PZ_247_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_247_MC_D2_491,
      O => NlwBufferSignal_N_PZ_247_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_247_MC_D1_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D1_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_247_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D1_IN2 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_N_PZ_247_MC_D1_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_0_IN3 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_0_IN3
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_1_IN5 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN5
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN5 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN5
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN6 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN6
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_2_IN7 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN7
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN5 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN5
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN6 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN6
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN7 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN7
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN8 : X_BUF
    port map (
      I => err_cs_or0003_471,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN8
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN9 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN9
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN10 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN10
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN11 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN11
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN12 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN12
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN13 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN13
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN14 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN14
    );
  NlwBufferBlock_N_PZ_247_MC_D2_PT_3_IN15 : X_BUF
    port map (
      I => Vcc_66,
      O => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN15
    );
  NlwBufferBlock_N_PZ_247_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_247_MC_D2_PT_0_492,
      O => NlwBufferSignal_N_PZ_247_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_247_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_247_MC_D2_PT_1_493,
      O => NlwBufferSignal_N_PZ_247_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_247_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_247_MC_D2_PT_2_494,
      O => NlwBufferSignal_N_PZ_247_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_247_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_247_MC_D2_PT_3_495,
      O => NlwBufferSignal_N_PZ_247_MC_D2_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D_IN0 : X_BUF
    port map (
      I => err_cs_or0003_MC_D1_498,
      O => NlwBufferSignal_err_cs_or0003_MC_D_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D_IN1 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_499,
      O => NlwBufferSignal_err_cs_or0003_MC_D_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => nClr_new_cs_500,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => nClr_old_cs_501,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_250_504,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_5_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_5_IN1 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_5_IN2 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_5_IN3 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_6_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_6_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_6_IN2 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_6_IN3 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_7_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_7_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_7_IN2 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_7_IN3 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_8_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_8_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_8_IN2 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_8_IN3 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_8_IN4 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN2 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN3 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN4 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_9_IN5 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN1 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN2 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN3 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN4 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_10_IN5 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN1 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN2 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN3 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN4 : X_BUF
    port map (
      I => err_cs_or000367_474,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_11_IN5 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN0 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN2 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN3 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN4 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_12_IN5 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN1 : X_BUF
    port map (
      I => N_PZ_137_70,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN3 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN4 : X_BUF
    port map (
      I => N_PZ_135_205,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN5 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_PT_13_IN6 : X_BUF
    port map (
      I => N_PZ_202_480,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN6
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN0 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_0_502,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN0
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN1 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_1_503,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN1
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN2 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_2_505,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN2
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN3 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_3_506,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN3
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN4 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_4_507,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN4
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN5 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_5_508,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN5
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN6 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_6_509,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN6
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN7 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_7_510,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN7
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN8 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_8_511,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN8
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN9 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_9_512,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN9
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN10 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_10_513,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN10
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN11 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_11_514,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN11
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN12 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_12_515,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN12
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN13 : X_BUF
    port map (
      I => err_cs_or0003_MC_D2_PT_13_516,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN13
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN14 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN14
    );
  NlwBufferBlock_err_cs_or0003_MC_D2_IN15 : X_BUF
    port map (
      I => Gnd_65,
      O => NlwBufferSignal_err_cs_or0003_MC_D2_IN15
    );
  NlwBufferBlock_nClr_new_cs_MC_REG_IN : X_BUF
    port map (
      I => nClr_new_cs_MC_D_518,
      O => NlwBufferSignal_nClr_new_cs_MC_REG_IN
    );
  NlwBufferBlock_nClr_new_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nClr_new_cs_MC_REG_CLK
    );
  NlwBufferBlock_nClr_new_cs_MC_D_IN0 : X_BUF
    port map (
      I => nClr_new_cs_MC_D1_519,
      O => NlwBufferSignal_nClr_new_cs_MC_D_IN0
    );
  NlwBufferBlock_nClr_new_cs_MC_D_IN1 : X_BUF
    port map (
      I => nClr_new_cs_MC_D2_520,
      O => NlwBufferSignal_nClr_new_cs_MC_D_IN1
    );
  NlwBufferBlock_nClr_new_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nClr_new_cs_MC_D1_IN0
    );
  NlwBufferBlock_nClr_new_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nClr_II_UIM_35,
      O => NlwBufferSignal_nClr_new_cs_MC_D1_IN1
    );
  NlwBufferBlock_nClr_old_cs_MC_REG_IN : X_BUF
    port map (
      I => nClr_old_cs_MC_D_522,
      O => NlwBufferSignal_nClr_old_cs_MC_REG_IN
    );
  NlwBufferBlock_nClr_old_cs_MC_REG_CLK : X_BUF
    port map (
      I => clk_II_FCLK_1,
      O => NlwBufferSignal_nClr_old_cs_MC_REG_CLK
    );
  NlwBufferBlock_nClr_old_cs_MC_D_IN0 : X_BUF
    port map (
      I => nClr_old_cs_MC_D1_523,
      O => NlwBufferSignal_nClr_old_cs_MC_D_IN0
    );
  NlwBufferBlock_nClr_old_cs_MC_D_IN1 : X_BUF
    port map (
      I => nClr_old_cs_MC_D2_524,
      O => NlwBufferSignal_nClr_old_cs_MC_D_IN1
    );
  NlwBufferBlock_nClr_old_cs_MC_D1_IN0 : X_BUF
    port map (
      I => nres_II_UIM_3,
      O => NlwBufferSignal_nClr_old_cs_MC_D1_IN0
    );
  NlwBufferBlock_nClr_old_cs_MC_D1_IN1 : X_BUF
    port map (
      I => nClr_new_cs_500,
      O => NlwBufferSignal_nClr_old_cs_MC_D1_IN1
    );
  NlwBufferBlock_N_PZ_163_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_163_MC_D1_527,
      O => NlwBufferSignal_N_PZ_163_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_163_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_163_MC_D2_528,
      O => NlwBufferSignal_N_PZ_163_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_163_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_163_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_ge0000_P_B_010_0114_134,
      O => NlwBufferSignal_N_PZ_163_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_163_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_163_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_N_PZ_163_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_163_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_163_MC_D2_PT_0_529,
      O => NlwBufferSignal_N_PZ_163_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_163_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_163_MC_D2_PT_1_530,
      O => NlwBufferSignal_N_PZ_163_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_202_MC_D1_533,
      O => NlwBufferSignal_N_PZ_202_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_202_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_202_MC_D2_534,
      O => NlwBufferSignal_N_PZ_202_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => Mcompar_err_cs_cmp_gt0000_P_B_010_0114_136,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_266_416,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => N_PZ_155_419,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_264_469,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_250_504,
      O => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_202_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_202_MC_D2_PT_0_535,
      O => NlwBufferSignal_N_PZ_202_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_202_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_202_MC_D2_PT_1_536,
      O => NlwBufferSignal_N_PZ_202_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_202_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_202_MC_D2_PT_2_537,
      O => NlwBufferSignal_N_PZ_202_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_250_MC_D1_540,
      O => NlwBufferSignal_N_PZ_250_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_541,
      O => NlwBufferSignal_N_PZ_250_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_163_479,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_1_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN3
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_1_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN4
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_9_Q_144,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_2_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN3
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_2_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN4
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_3_IN0 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_3_IN1 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_3_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_3_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN3
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_3_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN4
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_4_IN0 : X_BUF
    port map (
      I => cnt_ns_9_Q_147,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_4_IN1 : X_BUF
    port map (
      I => N_PZ_139_149,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_4_IN2 : X_BUF
    port map (
      I => cnt_v_mux0000_8_Q_151,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_4_IN3 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN3
    );
  NlwBufferBlock_N_PZ_250_MC_D2_PT_4_IN4 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN4
    );
  NlwBufferBlock_N_PZ_250_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_PT_0_542,
      O => NlwBufferSignal_N_PZ_250_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_250_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_PT_1_543,
      O => NlwBufferSignal_N_PZ_250_MC_D2_IN1
    );
  NlwBufferBlock_N_PZ_250_MC_D2_IN2 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_PT_2_544,
      O => NlwBufferSignal_N_PZ_250_MC_D2_IN2
    );
  NlwBufferBlock_N_PZ_250_MC_D2_IN3 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_PT_3_545,
      O => NlwBufferSignal_N_PZ_250_MC_D2_IN3
    );
  NlwBufferBlock_N_PZ_250_MC_D2_IN4 : X_BUF
    port map (
      I => N_PZ_250_MC_D2_PT_4_546,
      O => NlwBufferSignal_N_PZ_250_MC_D2_IN4
    );
  NlwBufferBlock_N_PZ_264_MC_D_IN0 : X_BUF
    port map (
      I => N_PZ_264_MC_D1_549,
      O => NlwBufferSignal_N_PZ_264_MC_D_IN0
    );
  NlwBufferBlock_N_PZ_264_MC_D_IN1 : X_BUF
    port map (
      I => N_PZ_264_MC_D2_550,
      O => NlwBufferSignal_N_PZ_264_MC_D_IN1
    );
  NlwBufferBlock_N_PZ_264_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_N_PZ_264_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_190_145,
      O => NlwBufferSignal_N_PZ_264_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_N_PZ_264_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => N_PZ_134_153,
      O => NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_N_PZ_264_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => cnt_ns_7_Q_357,
      O => NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_N_PZ_264_MC_D2_IN0 : X_BUF
    port map (
      I => N_PZ_264_MC_D2_PT_0_551,
      O => NlwBufferSignal_N_PZ_264_MC_D2_IN0
    );
  NlwBufferBlock_N_PZ_264_MC_D2_IN1 : X_BUF
    port map (
      I => N_PZ_264_MC_D2_PT_1_552,
      O => NlwBufferSignal_N_PZ_264_MC_D2_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D_IN0 : X_BUF
    port map (
      I => err_cs_or000367_MC_D1_555,
      O => NlwBufferSignal_err_cs_or000367_MC_D_IN0
    );
  NlwBufferBlock_err_cs_or000367_MC_D_IN1 : X_BUF
    port map (
      I => err_cs_or000367_MC_D2_556,
      O => NlwBufferSignal_err_cs_or000367_MC_D_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_0_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN0
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_0_IN1 : X_BUF
    port map (
      I => N_PZ_253_150,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_0_IN2 : X_BUF
    port map (
      I => err_cs_and000072_207,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN2
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_1_IN0 : X_BUF
    port map (
      I => summand_v_mux0001(10),
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN0
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_1_IN1 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_1_IN2 : X_BUF
    port map (
      I => SF4_204,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN2
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_2_IN0 : X_BUF
    port map (
      I => cnt_v_mux0000_5_Q_148,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN0
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_2_IN1 : X_BUF
    port map (
      I => SF87_160,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_PT_2_IN2 : X_BUF
    port map (
      I => N_PZ_138_197,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN2
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_IN0 : X_BUF
    port map (
      I => err_cs_or000367_MC_D2_PT_0_557,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_IN0
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_IN1 : X_BUF
    port map (
      I => err_cs_or000367_MC_D2_PT_1_558,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_IN1
    );
  NlwBufferBlock_err_cs_or000367_MC_D2_IN2 : X_BUF
    port map (
      I => err_cs_or000367_MC_D2_PT_2_559,
      O => NlwBufferSignal_err_cs_or000367_MC_D2_IN2
    );
  NlwInverterBlock_cnt_0_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D1_IN2,
      O => NlwInverterSignal_cnt_0_MC_D1_IN2
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_0_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_0_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_0_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_summand_v_mux0001_10_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_summand_v_mux0001_10_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_summand_v_mux0001_10_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_summand_v_mux0001_10_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_137_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_N_PZ_137_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_137_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_137_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_137_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_137_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_137_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_133_MC_D1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_133_MC_D1_IN0,
      O => NlwInverterSignal_N_PZ_133_MC_D1_IN0
    );
  NlwInverterBlock_nLd_new_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_nLd_new_cs_MC_D_IN0,
      O => NlwInverterSignal_nLd_new_cs_MC_D_IN0
    );
  NlwInverterBlock_nLd_new_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_nLd_new_cs_MC_D1_IN1,
      O => NlwInverterSignal_nLd_new_cs_MC_D1_IN1
    );
  NlwInverterBlock_nLd_old_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_nLd_old_cs_MC_D_IN0,
      O => NlwInverterSignal_nLd_old_cs_MC_D_IN0
    );
  NlwInverterBlock_nLd_old_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_nLd_old_cs_MC_D1_IN1,
      O => NlwInverterSignal_nLd_old_cs_MC_D1_IN1
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_10_BUFR_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_10_BUFR_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_cnt_10_BUFR_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_ge0000_P_B_010_0114_MC_D2_PT_2_IN6
    );
  NlwInverterBlock_cnt_ns_9_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D_IN0,
      O => NlwInverterSignal_cnt_ns_9_MC_D_IN0
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_ns_9_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_9_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_cnt_ns_9_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_cnt_v_mux0000_9_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_v_mux0000_9_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_v_mux0000_9_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_9_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_v_mux0000_5_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_v_mux0000_5_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_v_mux0000_5_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_5_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_5_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_5_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_5_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_138_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_138_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_138_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_138_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_138_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_138_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_err_cs_and000072_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_and000072_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_cs_and000072_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_cs_and000072_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_err_cs_and000072_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_and000072_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_cs_and000072_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_SF4_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_SF4_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_SF4_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_SF4_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_SF4_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_SF4_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_SF4_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_SF4_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_SF4_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_SF4_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_SF4_MC_D2_PT_1_IN5 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_1_IN5,
      O => NlwInverterSignal_SF4_MC_D2_PT_1_IN5
    );
  NlwInverterBlock_SF4_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_SF4_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_SF4_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_SF4_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_SF4_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_SF4_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_SF4_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_SF4_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_SF4_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_SF4_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_SF4_MC_D2_PT_3_IN5 : X_INV
    port map (
      I => NlwBufferSignal_SF4_MC_D2_PT_3_IN5,
      O => NlwInverterSignal_SF4_MC_D2_PT_3_IN5
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_1_MC_D2_PT_7_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_1_MC_D2_PT_7_IN5,
      O => NlwInverterSignal_cnt_1_MC_D2_PT_7_IN5
    );
  NlwInverterBlock_N_PZ_232_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_232_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_232_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_232_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_232_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_232_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_cnt_2_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_2_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_cnt_2_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_6_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_6_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_6_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_7_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_7_IN3,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_7_IN3
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_cnt_3_MC_D2_PT_8_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_3_MC_D2_PT_8_IN2,
      O => NlwInverterSignal_cnt_3_MC_D2_PT_8_IN2
    );
  NlwInverterBlock_N_PZ_135_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_135_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_135_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_4_BUFR_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_4_BUFR_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_cnt_4_BUFR_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_SF87_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_SF87_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_SF87_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_SF87_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_SF87_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_SF87_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_SF87_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_SF87_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_SF87_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_SF87_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_SF87_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_SF87_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_SF87_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_SF87_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_SF87_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_139_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_139_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_139_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_6_BUFR_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_6_BUFR_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_6_BUFR_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_253_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_253_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_253_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_253_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_v_mux0000_8_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_v_mux0000_8_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_cnt_v_mux0000_8_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_v_mux0000_8_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D1_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D1_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_2_IN5 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_2_IN5,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_2_IN5
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_cnt_8_BUFR_MC_D2_PT_3_IN4 : X_INV
    port map (
      I => NlwBufferSignal_cnt_8_BUFR_MC_D2_PT_3_IN4,
      O => NlwInverterSignal_cnt_8_BUFR_MC_D2_PT_3_IN4
    );
  NlwInverterBlock_cnt_ns_7_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_cnt_ns_7_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_7_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_cnt_ns_7_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_cnt_ns_7_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_cnt_ns_7_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_7_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_cnt_ns_7_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_cnt_ns_7_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_ns_7_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_ns_7_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_134_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_134_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_134_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_0_IN4
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN7
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_2_IN8
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN7
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_3_IN8
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_4_IN5
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN0
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN1
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN4
    );
  NlwInverterBlock_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5 : X_INV
    port map (
      I => NlwBufferSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5,
      O => NlwInverterSignal_Mcompar_err_cs_cmp_gt0000_P_B_010_0114_MC_D2_PT_5_IN5
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_1_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_1_IN4,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_1_IN4
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_190_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_190_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_N_PZ_190_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_cnt_11_BUFR_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_cnt_11_BUFR_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_cnt_11_BUFR_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_N_PZ_155_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_155_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_155_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_155_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_155_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_155_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_155_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_266_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_266_MC_D2_PT_0_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_266_MC_D2_PT_0_IN2,
      O => NlwInverterSignal_N_PZ_266_MC_D2_PT_0_IN2
    );
  NlwInverterBlock_N_PZ_266_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_266_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_266_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_266_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_266_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_5_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_5_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_5_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_6_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_6_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_6_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_7_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_7_IN0,
      O => NlwInverterSignal_err_MC_D2_PT_7_IN0
    );
  NlwInverterBlock_err_MC_D2_PT_8_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_8_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_8_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_9_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_9_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_9_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_10_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_10_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_10_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_10_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_10_IN4,
      O => NlwInverterSignal_err_MC_D2_PT_10_IN4
    );
  NlwInverterBlock_err_MC_D2_PT_10_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_10_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_10_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_11_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_11_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_11_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_11_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_11_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_11_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_11_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_11_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_11_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_12_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_12_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_12_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_12_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_12_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_12_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_12_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_12_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_12_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_12_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_12_IN6,
      O => NlwInverterSignal_err_MC_D2_PT_12_IN6
    );
  NlwInverterBlock_err_MC_D2_PT_13_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_13_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_13_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_13_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_13_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_13_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_13_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_13_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_13_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_13_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_13_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_13_IN7
    );
  NlwInverterBlock_err_MC_D2_PT_14_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_14_IN1,
      O => NlwInverterSignal_err_MC_D2_PT_14_IN1
    );
  NlwInverterBlock_err_MC_D2_PT_14_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_14_IN2,
      O => NlwInverterSignal_err_MC_D2_PT_14_IN2
    );
  NlwInverterBlock_err_MC_D2_PT_14_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_14_IN3,
      O => NlwInverterSignal_err_MC_D2_PT_14_IN3
    );
  NlwInverterBlock_err_MC_D2_PT_14_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_14_IN5,
      O => NlwInverterSignal_err_MC_D2_PT_14_IN5
    );
  NlwInverterBlock_err_MC_D2_PT_14_IN7 : X_INV
    port map (
      I => NlwBufferSignal_err_MC_D2_PT_14_IN7,
      O => NlwInverterSignal_err_MC_D2_PT_14_IN7
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_1_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_1_IN2,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_1_IN2
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_2_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN3,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN3
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_3_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN2,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN2
    );
  NlwInverterBlock_N_PZ_247_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_247_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_N_PZ_247_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_0_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_0_IN0,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_0_IN0
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_3_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_4_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_4_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_4_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_4_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_4_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_5_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_5_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_5_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_6_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_6_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_6_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_6_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_7_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_7_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_7_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_7_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_8_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_8_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_8_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_8_IN4,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_8_IN4
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_9_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_9_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_9_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN4,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN4
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_9_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_9_IN5,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_9_IN5
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_10_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_10_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_10_IN5,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_10_IN5
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_11_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_11_IN3 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN3,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN3
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_11_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_11_IN5,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_11_IN5
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_12_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN0,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN0
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_12_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_12_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN4,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN4
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_12_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_12_IN5,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_12_IN5
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_13_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN1,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN1
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_13_IN2 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN2,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN2
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_13_IN4 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN4,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN4
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_13_IN5 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN5,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN5
    );
  NlwInverterBlock_err_cs_or0003_MC_D2_PT_13_IN6 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or0003_MC_D2_PT_13_IN6,
      O => NlwInverterSignal_err_cs_or0003_MC_D2_PT_13_IN6
    );
  NlwInverterBlock_nClr_new_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_nClr_new_cs_MC_D_IN0,
      O => NlwInverterSignal_nClr_new_cs_MC_D_IN0
    );
  NlwInverterBlock_nClr_new_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_nClr_new_cs_MC_D1_IN1,
      O => NlwInverterSignal_nClr_new_cs_MC_D1_IN1
    );
  NlwInverterBlock_nClr_old_cs_MC_D_IN0 : X_INV
    port map (
      I => NlwBufferSignal_nClr_old_cs_MC_D_IN0,
      O => NlwInverterSignal_nClr_old_cs_MC_D_IN0
    );
  NlwInverterBlock_nClr_old_cs_MC_D1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_nClr_old_cs_MC_D1_IN1,
      O => NlwInverterSignal_nClr_old_cs_MC_D1_IN1
    );
  NlwInverterBlock_N_PZ_202_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_202_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_N_PZ_202_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_N_PZ_202_MC_D2_PT_1_IN0 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN0,
      O => NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN0
    );
  NlwInverterBlock_N_PZ_202_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_202_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_202_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_N_PZ_202_MC_D2_PT_2_IN2 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_202_MC_D2_PT_2_IN2,
      O => NlwInverterSignal_N_PZ_202_MC_D2_PT_2_IN2
    );
  NlwInverterBlock_N_PZ_250_MC_D2_PT_1_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_250_MC_D2_PT_1_IN3,
      O => NlwInverterSignal_N_PZ_250_MC_D2_PT_1_IN3
    );
  NlwInverterBlock_N_PZ_250_MC_D2_PT_2_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_250_MC_D2_PT_2_IN4,
      O => NlwInverterSignal_N_PZ_250_MC_D2_PT_2_IN4
    );
  NlwInverterBlock_N_PZ_250_MC_D2_PT_3_IN3 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_250_MC_D2_PT_3_IN3,
      O => NlwInverterSignal_N_PZ_250_MC_D2_PT_3_IN3
    );
  NlwInverterBlock_N_PZ_250_MC_D2_PT_4_IN4 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_250_MC_D2_PT_4_IN4,
      O => NlwInverterSignal_N_PZ_250_MC_D2_PT_4_IN4
    );
  NlwInverterBlock_N_PZ_264_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_N_PZ_264_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_N_PZ_264_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_cs_or000367_MC_D2_PT_0_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or000367_MC_D2_PT_0_IN1,
      O => NlwInverterSignal_err_cs_or000367_MC_D2_PT_0_IN1
    );
  NlwInverterBlock_err_cs_or000367_MC_D2_PT_1_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or000367_MC_D2_PT_1_IN1,
      O => NlwInverterSignal_err_cs_or000367_MC_D2_PT_1_IN1
    );
  NlwInverterBlock_err_cs_or000367_MC_D2_PT_2_IN0 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN0,
      O => NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN0
    );
  NlwInverterBlock_err_cs_or000367_MC_D2_PT_2_IN1 : X_INV
    port map (
      I => NlwBufferSignal_err_cs_or000367_MC_D2_PT_2_IN1,
      O => NlwInverterSignal_err_cs_or000367_MC_D2_PT_2_IN1
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => PRLD);

end Structure;

